<?php
$timeout = time()+60;
setcookie(SafetyForm, date("F jS - g:i a"), $timeout);
$name=$_POST['name'];
$from=$email=$_POST['email'];
$organization=$_POST['organization'];
$title=$_POST['title'];
$location=$_POST['location'];
$time=$_POST['time'];
$comments=$_POST['comments'];
$suggestion=$_POST['suggestion'];

$to="safety@metrojet.com";
//$to="sky@gopublic.com.hk";
$from = "safety@metrojet.com";
$subject = "Safety Feedback Form: $title";

// XSS prevention
$name = htmlspecialchars($name);
$organization = htmlspecialchars($organization);
$title = htmlspecialchars($title);
$location = htmlspecialchars($location);
$time = htmlspecialchars($time);
$comments = htmlspecialchars($comments);
$suggestion = htmlspecialchars($suggestion);

// XSS prevention
$name = strip_tags($name);
$organization= strip_tags($organization);
$title = strip_tags($title);
$location = strip_tags($location);
$time = strip_tags($time);
$comments = strip_tags($comments);
$suggestion = strip_tags($suggestion);

if ($name == "" || $email == "" || $organization == "") $name = $email = $organization = "Anonymous";
if ($suggestion == "") $suggestion = "N/A";

$message = "";
$message .= "Name: ".$name."\n";
$message .= "Email: ".$email."\n";
$message .= "Organization: ".$organization."\n";
$message .= "Title: ".$title."\n";
$message .= "Location: ".$location."\n";
$message .= "Time: ".$time."\n";
$message .= "Comments: ".$comments."\n";
$message .= "Suggestion: ".$suggestion."\n";
$error = "";
$success = "";

if (trim($title) == "" || trim($location) == "" || trim($time) == "" || trim($comments) == "") {
	$error = "Fail to submit. Please fill in all the fields before submitting the form.";
} elseif (isset($_COOKIE['SafetyForm'])) {
	$error = "Please wait for a few seconds to submit a new form.";
} else {
	if(empty($_FILES['photo']['tmp_name']) || $_FILES['photo']['tmp_name'] == 'none') { 
		if (mail($to, $subject, $message, "From: $email	\n"))
			$success = "";
		else
			$error = "Error in mail";
	} else {
		if(!empty($_FILES['photo']['error'])) {
			switch($_FILES['photo']['error']) {
				case '1':
					$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
					break;
				case '2':
					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
					break;
				case '3':
					$error = 'The uploaded file was only partially uploaded';
					break;
				case '4':
					$error = 'No file was uploaded.';
					break;
				case '6':
					$error = 'Missing a temporary folder';
					break;
				case '7':
					$error = 'Failed to write file to disk';
					break;
				case '8':
					$error = 'File upload stopped by extension';
					break;
				case '999':
				default:
					$error = 'No error code avaiable';
			}
		} else {
			$mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";
			$headers = "From: $name<$from>\r\n" .
					"Reply-To: $name<$from>\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: multipart/mixed;\r\n" .
					" boundary=\"{$mime_boundary}\"";

			$message = "This is a multi-part message in MIME format.\n\n" .
					"--{$mime_boundary}\n" .
					"Content-Type: text/plain; charset=\"iso-8859-1\"\n" .
					"Content-Transfer-Encoding: 7bit\n\n" .
					$message . "\n\n";

			foreach ($_FILES as $file) {
				$tmp_name = $file['tmp_name'];
				$type = $file['type'];
				$name = $file['name'];
				$size = $file['size'];

				if (file_exists($tmp_name)) {
					if(is_uploaded_file($tmp_name)) {
						$file = fopen($tmp_name,'rb');
		 
						$data = fread($file,filesize($tmp_name));
		 
						fclose($file);
		 
						$data = chunk_split(base64_encode($data));
					}
		 
					$message .= "--{$mime_boundary}\n" .
							"Content-Type: {$type};\n" .
							" name=\"{$name}\"\n" .
							"Content-Disposition: attachment;\n" .
							" filename=\"{$fileatt_name}\"\n" .
							"Content-Transfer-Encoding: base64\n\n" .
							$data . "\n\n";

				}
			}
			$message.="--{$mime_boundary}--\n";

			if (mail($to, $subject, $message, $headers))
				$success = "";
			else
				$error = "Error in mail";
		}
	}
}
echo "{error: '" . $error . "', success: '" . $success . "'}";
?>