<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Metrojet Corporate Flight Attendant Recruitment Form</title>

<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="../FARecruitmentForm/theme/css/styles.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="../FARecruitmentForm/theme/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../FARecruitmentForm/theme/css/shadowbox.css" />
<link rel="stylesheet" type="text/css" href="../FARecruitmentForm/theme/css/jquery.hoverscroll.css" />
<script type="text/javascript" src="../FARecruitmentForm/theme/js/jquery.min.js"></script>
<script type="text/javascript" src="../FARecruitmentForm/theme/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../FARecruitmentForm/theme/js/shadowbox.js"></script>
<script type="text/javascript" src="../FARecruitmentForm/theme/js/jquery.hoverscroll.js"></script>
<script type="text/javascript" src="../FARecruitmentForm/theme/js/ajaxfileupload.js"></script>
	
	<style type="text/css">
		textarea { width: 205px; height: 45px; font-family: Arial; font-size: 11px;}
		textarea:focus { width: 205px; height: 100px; font-family: Arial; font-size: 11px;}
		.defaultText { width: 205px; font-family: Arial;  font-size: 11px;}
		.defaultTextBrief { width: 500px; font-family: Arial;  font-size: 11px;}
		.defaultTextBrief:focus { width: 500px; height: 200px; font-family: Arial;  font-size: 11px;}
		.defaultTextRef { width: 300px; font-family: Arial;  font-size: 11px;}
		.defaultTextRef:focus { width: 380px; height: 200px; font-family: Arial;  font-size: 11px;}
		.defaultTextActive { color: #848484; font-style: italic; font-family: Arial; font-size: 11px; }
		div.box1
			{
				width:300px;
				padding:0px;
				border:1px solid black;
				margin:0px;
			}

		div.box2
			{
				width:385px;
				padding:0px;
				border:1px solid black;
				margin:0px;
			}			
		
		div.box3
			{
				width:705px;
				padding:0px;
				border:1px solid black;
				margin:0px;
			}			

		div.box4
			{
				width:705px;
				padding:0px;
				border:2px solid black;
				margin:0px;
			}
			
		.lefts {
			text-align:left;
		}

		.rights {
			text-align:right;
		}	
		
	</style>

	<script type="text/javascript" src="jquery.js">
	</script>
	
	<script type="text/javascript">
		function validateForm()
		{

		if (!document.auditForm.pics.checked)
			{ 
			alert("Please check the Personal Information Collection Statement. "); 
			return false;
			}
			
		var x=document.forms["auditForm"]["email"].value;
		if (x=="")
			{
			alert("Please fill in your email before submission.");
			document.forms["auditForm"]["email"].focus();
			if(document.all || document.getElementByID){
			document.forms["auditForm"]["email"].style.background = "#CCCCFF;";
			}

			return false;
			}
			
		var x=document.forms["auditForm"]["email"].value;
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if ( atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
			{
			alert("Please input valid email address");
			return false;
			}			
			
		var x=document.forms["auditForm"]["lastname"].value;
		if (x=="")
			{
			alert("Please fill in your Last name before submission.");
			return false;
			}
		var x=document.forms["auditForm"]["phone"].value;
		if (x=="")
			{
			alert("Please fill in your Phone Number before submission.");
			return false;
			}	
		var x=document.forms["auditForm"]["firstname"].value;
		if (x=="")
			{
			alert("Please fill in your First name before submission.");
			return false;
			}

		var x=document.forms["auditForm"]["dob"].value;
		if (x=="")
			{
			alert("Please fill in Date of Birth before submission.");
			return false;
			}				
		
		var x=document.forms["auditForm"]["nationality"].value;
		if (x=="")
			{
			alert("Please fill in your Nationality before submission.");
			return false;
			}	
		var x=document.forms["auditForm"]["current_address"].value;
		if (x=="")
			{
			alert("Please fill in your Current Address before submission.");
			return false;
			}	
		var x=document.forms["auditForm"]["home_address"].value;
		if (x=="")
			{
			alert("Please fill in your Home Address before submission.");
			return false;
			}		
		var x=document.forms["auditForm"]["eng_spoken"].value;
		if (x=="")
			{
			alert("Please fill in English Spoken field before submission.");
			return false;
			}				
		var x=document.forms["auditForm"]["eng_written"].value;
		if (x=="")
			{
			alert("Please fill in English Written field before submission.");
			return false;
			}							
		var x=document.forms["auditForm"]["mandarin_spoken"].value;
		if (x=="")
			{
			alert("Please fill in Mandarin Spoken field before submission.");
			return false;
			}					
		var x=document.forms["auditForm"]["mandarin_written"].value;
		if (x=="")
			{
			alert("Please fill in Mandarin Written field before submission.");
			return false;
			}						
		var x=document.forms["auditForm"]["cantonese_spoken"].value;
		if (x=="")
			{
			alert("Please fill in Cantonese Spoken field before submission.");
			return false;
			}						
		var x=document.forms["auditForm"]["cantonese_written"].value;
		if (x=="")
			{
			alert("Please fill in Cantonese Written field before submission.");
			return false;
			}
		var x=document.forms["auditForm"]["visa"].value;
		if (x=="")
			{
			alert("Please answer working VISA question before submission.");
			return false;
			}				
		var x=document.forms["auditForm"]["relocation"].value;
		if (x=="")
			{
			alert("Please answer the relocation question before submission.");
			return false;
			}				
		var x=document.forms["auditForm"]["experience"].value;
		if (x=="")
			{
			alert("Please answer the years of flying question before submission.");
			return false;
			}				
		var x=document.forms["auditForm"]["highestpos"].value;
		if (x=="")
			{
			alert("Please answer the highest postion held question before submission.");
			return false;
			}				
		var x=document.forms["auditForm"]["acqualified"].value;
		if (x=="")
			{
			alert("Please answer the qualified aircraft types question before submission.");
			return false;
			}				
		var x=document.forms["auditForm"]["education"].value;
		if (x=="")
			{
			alert("Please answer the education question before submission.");
			return false;
			}				
			var x=document.forms["auditForm"]["cv"].value;
		if (x=="")
			{
			alert("Please attach your CV before submission.");
			return false;
			}	
		var x=document.forms["auditForm"]["passportphoto"].value;
		if (x=="")
			{
			alert("Please attach your Passport Photo before submission.");
			return false;
			}		
			
		}
		
		
		
		$(document).ready(function()
		{
			$(".defaultText").focus(function(srcc)
			{
				if ($(this).val() == $(this)[0].title)
				{
					$(this).removeClass("defaultTextActive");
					$(this).val("");
				}
			});
			
			$(".defaultText").blur(function()
			{
				if ($(this).val() == "")
				{
					$(this).addClass("defaultTextActive");
					$(this).val($(this)[0].title);
				}
			});
			
			$(".defaultText").blur();        
		});
		
		$(window).load(function(){
		$("#firstname").change(function() {
		$("#fullname").val($("#firstname").val() + " " + $("#lastname").val()).change();
		})

		$("#lastname").change(function(){
		$("#fullname").val($("#firstname").val() + " " + $("#lastname").val()).change();
		})
		
		});
		
	</script>

	<link rel="stylesheet" href="../FARecruitmentForm/theme/css/chosen.css" />
	<style type="text/css" media="all">
    /* fix rtl for demo */
    .chzn-rtl .chzn-search { left: -9000px; }
    .chzn-rtl .chzn-drop { left: -9000px; }
	</style>
  
</head>
<body id="language_en">
<p>
	Welcome to the Metrojet Corporate Flight Attendant Recruitment section of the Metrojet website.&nbsp; Thank you for your expression of interest in a position with the Company.&nbsp; Please complete the form below.&nbsp;</p>

<section id="homeBodyContent">
	<table class="safety-form" align="left">
			<tr>
				<td class="left">
					<h2>Metrojet Corporate Flight Attendant Recruitment Form</h2></td>
<!--	
				<td class="left">
					<img src="http://www.metrojet.com/images/footer-logo.png" /></td>
-->					
			</tr>
		</table>
	<form action="mail.php" name="auditForm" method="post" accept-charset="UTF-8" enctype="multipart/form-data" onsubmit="return validateForm()">
	<table class="safety-form" align="left">
		<tbody>
			<tr>
				<td class="left"><label for="date">
					Date : 				
				</td>
				
					<?php
						$day=date("d");
						$month=date("F");
						$year=date("Y");
						$today = $month."/".$year;
						$today_ = "\"".$month."/".$year."\"";
						$date = $day."/".$month."/".$year;
						
						echo "<td class='right'><label for='date'>$date</td>";
					?>
				
			</tr>
			<tr>
				<td class="left">
					<label for="refno">Job Ref. No.</label>
				</td>
				<td class="right">
					<input id="refno" name="refno" type="text" />
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="email">Email</label>
				</td>
				<td class="right">
					<input name="email" type="text"/>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="lastname">Last Name</label>
				</td>
				<td class="right">
					<input id="lastname" name="lastname" type="text"  value="" />
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="firstname">First Name</label>
				</td>
				<td class="right">
					<input id="firstname" name="firstname" type="text"  value="" />
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="dob">DoB (DD/MM/YYYY)</label>
				</td>
				<td class="right">
					<input name="dob" type="text" />
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="chinesename">Chinese Name<br>(In Chinese, if applicable)</label>
				</td>
				<td class="right">
					<input name="chinesename" type="text"/>
					<input name="ieutf8check" type="hidden" value="&#9760;" />
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="phone">Phone Number <br>(Country Code)(Area Code) </label>
				</td>
				<td class="right">
					<input name="phone" type="text"/>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="nationality">Nationality </label>
				</td>
				<td class="right">
					<input name="nationality" type="text"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="left">
					<label for="language">Language</label>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;English (Spoken) </label>
				</td>
				<td class="right">
					<select name="eng_spoken" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;English (Written) </label>
				</td>
				<td class="right">
					<select name="eng_written" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>	
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;Mandarin (Spoken) </label>
				</td>
				<td class="right">
					<select name="mandarin_spoken" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;Simplified Chinese (Written) </label>
				</td>
				<td class="right">
					<select name="mandarin_written" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>				
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;Cantonese (Spoken) </label>
				</td>
				<td class="right">
					<select name="cantonese_spoken" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;Traditional Chinese (Written) </label>
				</td>
				<td class="right">
					<select name="cantonese_written" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;Others (Spoken) </label> <input name="others_spoken1" type="text"/>
				</td>
				<td class="right">
					<select name="others_spoken" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="language">&nbsp;&nbsp;&nbsp;&nbsp;Others (Written) </label> <input name="others_written1" type="text"/>
				</td>
				<td class="right">
					<select name="others_written" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Excellence">Excellent</option>
					<option value="Good">Good</option>
					<option value="Basic">Basic</option>  
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="current_address">Current Address</label>
				</td>
				<td class="right">
					<textarea wrap="physical" class="defaultTextRef" id="current_address" name="current_address" ></textarea>					
				</td>
			</tr>						
			<tr>
				<td class="left">
					<label for="home_address">Home Address</label>
				</td>
				<td class="right">
					<textarea wrap="physical" class="defaultTextRef" id="home_address" name="home_address" ></textarea>					
				</td>
			</tr>		
			<tr>
				<td class="left">
					<label for="visa">Do you need a visa to work in Hong Kong? </label>
				</td>
				<td class="right">
					<select name="visa" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="relocation">Are you prepared for relocation? </label>
				</td>
				<td class="right">
					<select name="relocation" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="experience">Years of Flying Experience? </label>
				</td>
				<td class="right">
					<select name="experience" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25 Above">25+</option>
				</td>
			</tr>		
			<tr>
				<td class="left">
					<label for="highestpos">Highest Position Held </label>
				</td>
				<td class="right">
					<input name="highestpos" type="text"/>
				</td>
			</tr>		
			<tr>
				<td class="left">
					<label for="acqualified">Aircraft types with experience on </label>
				</td>
				<td class="right">
					<input name="acqualified" type="text"/>
				</td>
			</tr>					
			<tr>
				<td class="left">
					<label for="education">Educational Qualifications</label>
				</td>
				<td class="right">
					<select name="education" class="chzn-select" tabindex="2">
					<option value="">...</option>
					<option value="Post-graduate">Post-graduate</option>
					<option value="University">University</option>
					<option value="High_School">High School</option>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label for="otherqual">Other Qualifications (Please Specify)  </label>
				</td>
				<td class="right">
					<input name="otherqual" type="text"/>
				</td>
			</tr>	
			<tr>
				<td class="left">
					<label for="otherqualyear">&nbsp;&nbsp;&nbsp;&nbsp;Year Obtained  </label>
				</td>
				<td class="right">
					<input name="otherqualyear" type="text"/>
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="cv">Please attach your CV <br>( PDF file not exceeding 2MB in size)  </label>
				</td>
				<td class="right">						
					<input type="file" id="cv" name="cv" />
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="passportphoto">Please attach a passport photo <br>(jpeg file resolution 600x600 pixels or above) </label>
				</td>
				<td class="right">						
					<input type="file" id="passportphoto" name="passportphoto" />
				</td>
			</tr>			
			<tr>
				<td class="left">
					<label for="fullphoto">Please attach a Full Length photo <br>(jpeg file not exceeding 2MB) </label>
				</td>
				<td class="right">						
					<input type="file" id="fullphoto" name="fullphoto" />
				</td>
			</tr>					
			<tr>
				<td colspan="2" class="left">
					&nbsp;
				</td>
			</tr>					
			<tr>
				<td colspan="2" class="left">
					<p align="justify"><input type="checkbox" id="pics" name="pics" value="Yes">&nbsp;&nbsp;<b>Personal Information Collection Statement </b><br>
						Personal data provided by applicants will be used by Metrojet to assess the applicant&#39;s suitability for the position for which they have applied or, in the case of an unsolicited expression of interest, for any other suitable vacancy.<br><br>
						We will retain the personal data of applicants for a period of 12 months.  For applicants who are former employees of Metrojet, we may retrieve any available employment-related records for reference.<br><br>
						Under the Personal Data (Privacy) Ordinance, any applicant has the right to request access to, and request correction of, personal data in relation to his/her application. Any applicant who wishes to exercise these rights should use the &#39;Contact Us&#39; facility to request a "Data Access Request Form" which should be completed and forwarded to our HR Department.  
					</p>
				</td>
			</tr>					
			<tr>
				<td class="left">
					&nbsp;
				</td>
				<td class="right">	
					<table>
						<tbody>
							<tr>
								<td width="50%">
								&nbsp;&nbsp;&nbsp;
								</td>
								<td width="20%">	
									<div class="button">
									<input type="submit" name="Submit" value="Submit" /></div>
								</td>
								<td width="30%">
								&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</tbody>
					</table>
				</td>	
			</tr>						
			</tbody>
	</table>

<!--	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
	<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
	<script type="text/javascript"> 
    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
	</script>
-->
	
	</form>
</section>
							
<!--
<footer>
	<center>
		<p><a href="mailto:pilotjobquery@metrojet.com">Contact Us</a> | 
		Copyright &copy; 2014 Metrojet Limited All rights reserved</p>
	</center>
</footer>
-->

</body>
</html>