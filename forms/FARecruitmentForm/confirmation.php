<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<body bgcolor="f6f1e5">
	<title>Metrojet Corporate Flight Attendant Recruitment</title>
	
</head>

<font face="Calibri">

<section id="homeBodyContent" width="700px">
	<div class="Heading">
		<table class="page-head" align="center" width="700px">
			<tr>
				<td class="left">
					<h1>Metrojet Corporate Flight Attendant Recruitment<br></h1></td>
				<td class="right" width="63">
					<img src="http://www.metrojet.com/images/footer-logo.png" /></td>
			</tr>
		</table>
	</div>
	<div class="leftColWrap">
			<table class="audit-table" align="center" style="border:1px solid black;" width="700px">
				<tbody>
					<tr> 
						<td align="center" valign="middle" bgcolor="#f5eed3"><p>Thank you for completing the online application for a position with Metrojet.  For unsolicited applications that are not in response to a specific job advertisement, your details will be retained for a period of one year, after which they will be destroyed.  If you have not heard from us within that 12-month period, you may feel free to resubmit your expression of interest via the website template with updated details.  If your application is in response to a specific advertisement, you can expect to hear from us within 2 weeks of the date of application.  </p></td>
					</tr>
				</tbody>
			</table>
	</div>
</section>
<footer>
	<center>
		<p><a href="mailto:pilotjobquery@metrojet.com">Contact Us</a> | 
		Copyright &copy; 2014 Metrojet Limited All rights reserved</p>
	</center>
</footer>

</body>
</html>