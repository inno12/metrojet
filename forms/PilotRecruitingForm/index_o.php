<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Metrojet Pilot Recruitment</title>
	
	<style type="text/css">
		textarea { width: 205px; height: 45px; font-family: Arial; font-size: 14px;}
		textarea:focus { width: 205px; height: 100px; font-family: Arial; font-size: 14px;}
		.defaultText { width: 205px; font-family: Arial;  font-size: 14px;}
		.defaultTextBrief { width: 500px; font-family: Arial;  font-size: 14px;}
		.defaultTextBrief:focus { width: 500px; height: 200px; font-family: Arial;  font-size: 14px;}
		.defaultTextRef { width: 380px; font-family: Arial;  font-size: 14px;}
		.defaultTextRef:focus { width: 380px; height: 200px; font-family: Arial;  font-size: 14px;}
		.defaultTextActive { color: #848484; font-style: italic; font-family: Arial; font-size: 14px; }
		div.box1
			{
				width:300px;
				padding:0px;
				border:1px solid black;
				margin:0px;
			}

		div.box2
			{
				width:385px;
				padding:0px;
				border:1px solid black;
				margin:0px;
			}			
		
		div.box3
			{
				width:705px;
				padding:0px;
				border:1px solid black;
				margin:0px;
			}			

		div.box4
			{
				width:705px;
				padding:0px;
				border:2px solid black;
				margin:0px;
			}
			
		.lefts {
			text-align:left;
		}

		.rights {
			text-align:right;
		}	
		
	</style>

	<script type="text/javascript">
	
		function updatesum() {
		var numd6 = parseFloat(document.getElementById('d6').value);
		var nume6 = parseFloat(document.getElementById('e6').value);
		var numf6 = parseFloat(document.getElementById('f6').value);
		var numg6 = parseFloat(document.getElementById('g6').value);
		var numh6 = parseFloat(document.getElementById('h6').value);
		var numi6 = parseFloat(document.getElementById('i6').value);
		var numj6 = parseFloat(document.getElementById('j6').value);
		var numd7 = parseFloat(document.getElementById('d7').value);
		var nume7 = parseFloat(document.getElementById('e7').value);
		var numf7 = parseFloat(document.getElementById('f7').value);
		var numg7 = parseFloat(document.getElementById('g7').value);
		var numh7 = parseFloat(document.getElementById('h7').value);
		var numi7 = parseFloat(document.getElementById('i7').value);
		var numj7 = parseFloat(document.getElementById('j7').value);
		var numd8 = parseFloat(document.getElementById('d8').value);
		var nume8 = parseFloat(document.getElementById('e8').value);
		var numf8 = parseFloat(document.getElementById('f8').value);
		var numg8 = parseFloat(document.getElementById('g8').value);
		var numh8 = parseFloat(document.getElementById('h8').value);
		var numi8 = parseFloat(document.getElementById('i8').value);
		var numj8 = parseFloat(document.getElementById('j8').value);
		var numd9 = parseFloat(document.getElementById('d9').value);
		var nume9 = parseFloat(document.getElementById('e9').value);
		var numf9 = parseFloat(document.getElementById('f9').value);
		var numg9 = parseFloat(document.getElementById('g9').value);
		var numh9 = parseFloat(document.getElementById('h9').value);
		var numi9 = parseFloat(document.getElementById('i9').value);
		var numj9 = parseFloat(document.getElementById('j9').value);
		var numd10 = parseFloat(document.getElementById('d10').value);
		var nume10 = parseFloat(document.getElementById('e10').value);
		var numf10 = parseFloat(document.getElementById('f10').value);
		var numg10 = parseFloat(document.getElementById('g10').value);
		var numh10 = parseFloat(document.getElementById('h10').value);
		var numi10 = parseFloat(document.getElementById('i10').value);
		var numj10 = parseFloat(document.getElementById('j10').value);
		var numd11 = parseFloat(document.getElementById('d11').value);
		var nume11 = parseFloat(document.getElementById('e11').value);
		var numf11 = parseFloat(document.getElementById('f11').value);
		var numg11 = parseFloat(document.getElementById('g11').value);
		var numh11 = parseFloat(document.getElementById('h11').value);
		var numi11 = parseFloat(document.getElementById('i11').value);
		var numj11 = parseFloat(document.getElementById('j11').value);		
		var numd12 = parseFloat(document.getElementById('d12').value);
		var nume12 = parseFloat(document.getElementById('e12').value);
		var numf12 = parseFloat(document.getElementById('f12').value);
		var numg12 = parseFloat(document.getElementById('g12').value);
		var numh12 = parseFloat(document.getElementById('h12').value);
		var numi12 = parseFloat(document.getElementById('i12').value);
		var numj12 = parseFloat(document.getElementById('j12').value);
		var numd13 = parseFloat(document.getElementById('d13').value);
		var nume13 = parseFloat(document.getElementById('e13').value);
		var numf13 = parseFloat(document.getElementById('f13').value);
		var numg13 = parseFloat(document.getElementById('g13').value);
		var numh13 = parseFloat(document.getElementById('h13').value);
		var numi13 = parseFloat(document.getElementById('i13').value);
		var numj13 = parseFloat(document.getElementById('j13').value);
		var numd14 = parseFloat(document.getElementById('d14').value);
		var nume14 = parseFloat(document.getElementById('e14').value);
		var numf14 = parseFloat(document.getElementById('f14').value);
		var numg14 = parseFloat(document.getElementById('g14').value);
		var numh14 = parseFloat(document.getElementById('h14').value);
		var numi14 = parseFloat(document.getElementById('i14').value);
		var numj14 = parseFloat(document.getElementById('j14').value);
		var numd15 = parseFloat(document.getElementById('d15').value);
		var nume15 = parseFloat(document.getElementById('e15').value);
		var numf15 = parseFloat(document.getElementById('f15').value);
		var numg15 = parseFloat(document.getElementById('g15').value);
		var numh15 = parseFloat(document.getElementById('h15').value);
		var numi15 = parseFloat(document.getElementById('i15').value);
		var numj15 = parseFloat(document.getElementById('j15').value);		
		var numd16 = parseFloat(document.getElementById('d16').value);
		var nume16 = parseFloat(document.getElementById('e16').value);
		var numf16 = parseFloat(document.getElementById('f16').value);
		var numg16 = parseFloat(document.getElementById('g16').value);
		var numh16 = parseFloat(document.getElementById('h16').value);
		var numi16 = parseFloat(document.getElementById('i16').value);
		var numj16 = parseFloat(document.getElementById('j16').value);		
		var numd17 = parseFloat(document.getElementById('d17').value);
		var nume17 = parseFloat(document.getElementById('e17').value);
		var numf17 = parseFloat(document.getElementById('f17').value);
		var numg17 = parseFloat(document.getElementById('g17').value);
		var numh17 = parseFloat(document.getElementById('h17').value);
		var numi17 = parseFloat(document.getElementById('i17').value);
		var numj17 = parseFloat(document.getElementById('j17').value);		
		var numd18 = parseFloat(document.getElementById('d18').value);
		var nume18 = parseFloat(document.getElementById('e18').value);
		var numf18 = parseFloat(document.getElementById('f18').value);
		var numg18 = parseFloat(document.getElementById('g18').value);
		var numh18 = parseFloat(document.getElementById('h18').value);
		var numi18 = parseFloat(document.getElementById('i18').value);
		var numj18 = parseFloat(document.getElementById('j18').value);				
		var numd19 = parseFloat(document.getElementById('d19').value);
		var nume19 = parseFloat(document.getElementById('e19').value);
		var numf19 = parseFloat(document.getElementById('f19').value);
		var numg19 = parseFloat(document.getElementById('g19').value);
		var numh19 = parseFloat(document.getElementById('h19').value);
		var numi19 = parseFloat(document.getElementById('i19').value);
		var numj19 = parseFloat(document.getElementById('j19').value);						
		var numd20 = parseFloat(document.getElementById('d20').value);
		var nume20 = parseFloat(document.getElementById('e20').value);
		var numf20 = parseFloat(document.getElementById('f20').value);
		var numg20 = parseFloat(document.getElementById('g20').value);
		var numh20 = parseFloat(document.getElementById('h20').value);
		var numi20 = parseFloat(document.getElementById('i20').value);
		var numj20 = parseFloat(document.getElementById('j20').value);				
		var numd21 = parseFloat(document.getElementById('d21').value);
		var nume21 = parseFloat(document.getElementById('e21').value);
		var numf21 = parseFloat(document.getElementById('f21').value);
		var numg21 = parseFloat(document.getElementById('g21').value);
		var numh21 = parseFloat(document.getElementById('h21').value);
		var numi21 = parseFloat(document.getElementById('i21').value);
		var numj21 = parseFloat(document.getElementById('j21').value);						
		
		document.getElementById('j6').value = (numd6 + nume6 + numf6).toFixed(1);
		document.getElementById('j7').value = (numd7 + nume7 + numf7).toFixed(1);
		document.getElementById('j8').value = (numd8 + nume8 + numf8).toFixed(1);
		document.getElementById('j9').value = (numd9 + nume9 + numf9).toFixed(1);
		document.getElementById('j10').value = (numd10 + nume10 + numf10).toFixed(1);
		document.getElementById('j11').value = (numd11 + nume11 + numf11).toFixed(1);
		document.getElementById('j12').value = (numd12 + nume12 + numf12).toFixed(1);
		document.getElementById('j13').value = (numd13 + nume13 + numf13).toFixed(1);
		document.getElementById('j14').value = (numd14 + nume14 + numf14).toFixed(1);

		document.getElementById('d15').value = (numd6 + numd7 + numd8).toFixed(1);
		document.getElementById('e15').value = (nume6 + nume7 + nume8).toFixed(1);
		document.getElementById('f15').value = (numf6 + numf7 + numf8).toFixed(1);
		document.getElementById('g15').value = (numg6 + numg7 + numg8).toFixed(1);
		document.getElementById('h15').value = (numh6 + numh7 + numh8).toFixed(1);
		document.getElementById('i15').value = (numi6 + numi7 + numi8).toFixed(1);
		document.getElementById('j15').value = (numj6 + numj7 + numj8).toFixed(1);
		
		document.getElementById('d16').value = (numd9 + numd12).toFixed(1);
		document.getElementById('e16').value = (nume9 + nume12).toFixed(1);
		document.getElementById('f16').value = (numf9 + numf12).toFixed(1);
		document.getElementById('g16').value = (numg9 + numg12).toFixed(1);
		document.getElementById('h16').value = (numh9 + numh12).toFixed(1);
		document.getElementById('i16').value = (numi9 + numi12).toFixed(1);
		document.getElementById('j16').value = (numj9 + numj12).toFixed(1);
		
		document.getElementById('d17').value = (numd6 + numd7 + numd8 + numd9 + numd10).toFixed(1);
		document.getElementById('e17').value = (nume6 + nume7 + nume8 + nume9 + nume10).toFixed(1);
		document.getElementById('f17').value = (numf6 + numf7 + numf8 + numf9 + numf10).toFixed(1);
		document.getElementById('g17').value = (numg6 + numg7 + numg8 + numg9 + numg10).toFixed(1);
		document.getElementById('h17').value = (numh6 + numh7 + numh8 + numh9 + numh10).toFixed(1);
		document.getElementById('i17').value = (numi6 + numi7 + numi8 + numi9 + numi10).toFixed(1);
		document.getElementById('j17').value = (numj6 + numj7 + numj8 + numj9 + numj10).toFixed(1);
		
		document.getElementById('d18').value = (numd11 + numd12 + numd13).toFixed(1);
		document.getElementById('e18').value = (nume11 + nume12 + nume13).toFixed(1);
		document.getElementById('f18').value = (numf11 + numf12 + numf13).toFixed(1);
		document.getElementById('g18').value = (numg11 + numg12 + numg13).toFixed(1);
		document.getElementById('h18').value = (numh11 + numh12 + numh13).toFixed(1);
		document.getElementById('i18').value = (numi11 + numi12 + numi13).toFixed(1);
		document.getElementById('j18').value = (numj11 + numj12 + numj13).toFixed(1);
		
		document.getElementById('d19').value = (numd6 + numd7 + numd8 + numd9 + numd10 + numd11 + numd12 + numd13).toFixed(1);
		document.getElementById('e19').value = (nume6 + nume7 + nume8 + nume9 + nume10 + nume11 + nume12 + nume13).toFixed(1);
		document.getElementById('f19').value = (numf6 + numf7 + numf8 + numf9 + numf10 + numf11 + numf12 + numf13).toFixed(1);
		document.getElementById('g19').value = (numg6 + numg7 + numg8 + numg9 + numg10 + numg11 + numg12 + numg13).toFixed(1);
		document.getElementById('h19').value = (numh6 + numh7 + numh8 + numh9 + numh10 + numh11 + numh12 + numh13).toFixed(1);
		document.getElementById('i19').value = (numi6 + numi7 + numi8 + numi9 + numi10 + numi11 + numi12 + numi13).toFixed(1);
		document.getElementById('j19').value = (numj6 + numj7 + numj8 + numj9 + numj10 + numj11 + numj12 + numj13).toFixed(1);
		
		document.getElementById('d20').value = (numd14).toFixed(1);
		document.getElementById('e20').value = (nume14).toFixed(1);
		document.getElementById('f20').value = (numf14).toFixed(1);
		document.getElementById('g20').value = (numg14).toFixed(1);
		document.getElementById('h20').value = (numh14).toFixed(1);
		document.getElementById('i20').value = (numi14).toFixed(1);
		document.getElementById('j20').value = (numj14).toFixed(1);
		
		document.getElementById('d21').value = (numd6 + numd7 + numd8 + numd9 + numd10 + numd11 + numd12 + numd13 + numd14).toFixed(1);
		document.getElementById('e21').value = (nume6 + nume7 + nume8 + nume9 + nume10 + nume11 + nume12 + nume13 + nume14).toFixed(1);
		document.getElementById('f21').value = (numf6 + numf7 + numf8 + numf9 + numf10 + numf11 + numf12 + numf13 + numf14).toFixed(1);
		document.getElementById('g21').value = (numg6 + numg7 + numg8 + numg9 + numg10 + numg11 + numg12 + numg13 + numg14).toFixed(1);
		document.getElementById('h21').value = (numh6 + numh7 + numh8 + numh9 + numh10 + numh11 + numh12 + numh13 + numh14).toFixed(1);
		document.getElementById('i21').value = (numi6 + numi7 + numi8 + numi9 + numi10 + numi11 + numi12 + numi13 + numi14).toFixed(1);
		document.getElementById('j21').value = (numj6 + numj7 + numj8 + numj9 + numj10 + numj11 + numj12 + numj13 + numj14).toFixed(1);
		
		}

		function updateType() {
		
		var	StringA = document.getElementById('TypeA').value;
		var	StringB = document.getElementById('TypeB').value;
		
		document.getElementById('TypeA_t').value = StringA;
		document.getElementById('TypeB_t').value = StringB;
		
		document.getElementById('c6').value = StringA;
		document.getElementById('c7').value = StringB;
		
		}
	</script>						
	
	<script type="text/javascript" src="jquery.js">
	</script>
	
	<script type="text/javascript">
		
		$(document).ready(function()
		{
			$(".defaultText").focus(function(srcc)
			{
				if ($(this).val() == $(this)[0].title)
				{
					$(this).removeClass("defaultTextActive");
					$(this).val("");
				}
			});
			
			$(".defaultText").blur(function()
			{
				if ($(this).val() == "")
				{
					$(this).addClass("defaultTextActive");
					$(this).val($(this)[0].title);
				}
			});
			
			$(".defaultText").blur();        
		});
		
		$(window).load(function(){
		$("#firstname").change(function() {
		$("#fullname").val($("#firstname").val() + " " + $("#lastname").val()).change();
		})

		$("#lastname").change(function(){
		$("#fullname").val($("#firstname").val() + " " + $("#lastname").val()).change();
		})
		
		});
		
	</script>

	<link rel="stylesheet" href="chosen/chosen.css" />
	<style type="text/css" media="all">
    /* fix rtl for demo */
    .chzn-rtl .chzn-search { left: -9000px; }
    .chzn-rtl .chzn-drop { left: -9000px; }
	</style>
  
</head>
<body bgcolor="f6f1e5">
<font size="3" face="Arial">

<section id="homeBodyContent" width="700">
	<div class="Heading">
		<table class="page-head" align="center" width="700">
			<tr>
				<td class="left">
					<h1>Metrojet Pilot Recruitment Form</h1></td>
				<td class="right" width="63">
					<img src="http://www.metrojet.com/images/footer-logo.png" /></td>
			</tr>
		</table>
	</div>
	<div class="leftColWrap">
		<form action="mail_o.php" name="auditForm" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
			<table class="audit-background" align="center" width="700">
				<tbody>
					<tr>
						<td class="left" ><label for="date">Date : </td>
<?php
	$day=date("d");
	$month=date("F");
	$year=date("Y");
	$today = $month."/".$year;
	$today_ = "\"".$month."/".$year."\"";
	$date = $day."/".$month."/".$year;
	
	echo "<td colspan='4' class='left' ><label for='date'>$date</td>";
?>					

					</tr>
				
				
				
					<tr>
						<td class="left" style="vertical-align: top; width: 15%;">
							<label for="refno">Job Ref. No.:</label></td>
						<td class="left" style="vertical-align: top; width: 30%;" >
							<input name="refno" type="text"  size="30" /></td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td class="right" style="vertical-align: top; width: 20%;" >
							<label for="email">Email</label></td>
						<td class="left" style="vertical-align: top; width: 30%;">
							<input name="email" type="text"  size="30" /></td>	
					</tr>
					<tr>
						<td class="left" style="vertical-align: top; width: 15%;">
							<label for="lastname">Last Name</label></td>
						<td class="right" style="vertical-align: top; width: 30%;" >
							<input id="lastname" name="lastname" type="text"  size="30" value="" /></td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td class="left" style="vertical-align: top; width: 20%;" >
							<label for="phone">Phone Number <br>(Country Code)(Area Code) </label></td>
						<td class="right" style="vertical-align: top; width: 30%;">
							<input name="phone" type="text"  size="30" /></td>	
					</tr>
					<tr>
						<td class="left" style="vertical-align: top; width: 15%;">
							<label for="firstname">First Name</label></td>
						<td class="right" style="vertical-align: top; width: 30%;" >
							<input id="firstname" name="firstname" type="text"  size="30" value="" /></td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td class="left" style="vertical-align: top; width: 20%;" >
							<label for="dob">DoB (DD/MM/YYYY)</label></td>
						<td class="right" style="vertical-align: top; width: 30%;">
							<input name="dob" type="text"  size="30" /></td>	
					</tr>				
					<tr>
						<td class="left" style="vertical-align: top; width: 15%;">
							<label for="middlename">Middle Name</label></td>
						<td class="right" style="vertical-align: top; width: 30%;" >
							<input name="middlename" type="text"  size="30" /></td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td class="left" style="vertical-align: top; width: 20%;" >
							<label for="nationality">Nationality </label></td>
						<td class="right" style="vertical-align: top; width: 30%;">
							<input name="nationality" type="text"  size="30" /></td>	
					</tr>									
					<tr>
						<td class="left" style="vertical-align: top; width: 15%;">
							<label for="address">Address </label></td>
						<td class="right" style="vertical-align: top; width: 30%;" >
						<textarea wrap="physical" class="defaultText" id="address" name="address" ></textarea></td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td class="left" style="vertical-align: top; width: 20%;" >
							<label for="lang">Languages Spoken/Written</label></td>
						<td class="right" style="vertical-align: top; width: 30%;">
						<textarea wrap="physical" class="defaultText" id="lang" name="lang" ></textarea></td>	
					</tr>	
					<tr>
						<td colspan="2" class="left" style="vertical-align: top; width: 45%;">
						<table>
						<tr>
							<td class="left" >
							<label for="icao">English Level &nbsp;&nbsp;</label>
							</td>
							<td>
							<select name="icao" style="width:183px;" class="chzn-select" tabindex="2">
							<option value="">...</option>
							<option value="Native Speaker">Native Speaker</option>
							<option value="ICAO 1">ICAO Level 1</option>
							<option value="ICAO 2">ICAO Level 2</option>     
							<option value="ICAO 3">ICAO Level 3</option>     
							<option value="ICAO 4">ICAO Level 4</option>     
							<option value="ICAO 5">ICAO Level 5</option>
							<option value="ICAO 6">ICAO Level 6</option>
							</td>	
						</tr>
						<tr>	
							<td class="left" >
							<label for="mandarin">Mandarin Level &nbsp;&nbsp;</label>
							</td>
							<td>
							<select name="mandarin" style="width:183px;" class="chzn-select" tabindex="2">
							<option value="">...</option>
							<option value="Native Speaker">Native Speaker</option>
							<option value="HSK Level 1">HSK Level 1</option>
							<option value="HSK Level 2">HSK Level 2</option>     
							<option value="HSK Level 3">HSK Level 3</option>     
							<option value="HSK Level 4">HSK Level 4</option>     
							<option value="HSK Level 5">HSK Level 5</option>
							<option value="HSK Level 6">HSK Level 6</option>
							<option value="Not Applicable">Not Applicable</option>
							</td>
						</tr>
						</table>
						</td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td class="left" style="vertical-align: top; width: 20%;" >
							<label for="edu">Educational Qualifications: </label></td>
						<td class="right" style="vertical-align: top; width: 30%;"><textarea wrap="physical" class="defaultText" id="edu" name="edu" ></textarea></td>	
						</tr>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="vertical-align: top; width: 45%;">
							<div class="box1">
							<table width="100%">
							<tr>
								<td class="left" style="vertical-align: top; width: 70%;" >
									<label for="class1medical">Class 1 Medical&nbsp;&nbsp;</label>
								</td>	
								<td class="left" style="vertical-align: top; width: 30%;" >
									<select name="class1medical" style="width:130px;" class="chzn-select" tabindex="2">
									<option value="">...</option>
									<option value="Yes">Yes</option>
									<option value="No">No</option>
								</td>	
							</tr>
							<tr>
								<td class="left" style="vertical-align: top; width: 70%;" >
									<label for="licence">Licence&nbsp;&nbsp;</label>
								</td>
								<td class="left" style="vertical-align: top; width: 30%;" >
									<select name="licence" style="width:130px;" class="chzn-select" tabindex="2">
									<option value="">...</option>
									<option value="ATPL">ATPL</option>
									<option value="CPL">CPL</option>
								</td>
							</tr>
							<tr>
								<td class="left" style="vertical-align: top; width: 70%;" >
								<label for="lic_auth">Licensing Authority&nbsp;&nbsp;&nbsp;</label>
								</td>
								<td class="left" style="vertical-align: top; width: 30%;" >
								<input name="lic_auth" type="text"  size="16" />
								</td>
							</tr>
							<tr>
								<td class="left" style="vertical-align: top; width: 70%;" >
									<label for="radio_permit">Radiotelephony Permit&nbsp;&nbsp;&nbsp;</label>
								</td>
								<td class="left" style="vertical-align: top; width: 30%;" >
									<select name="radio_permit" style="width:130px;" class="chzn-select" tabindex="2">
									<option value="">...</option>
									<option value="Yes">Yes</option>
									<option value="No">No</option>
								</td>
							</tr>
							</table>
							</div>
							<br>
							<div class="box1">
							<table>
							<tr>
								<td colspan="2" class="left" style="vertical-align: top; width: 100%;">
								<label for="Type">Current Multi Jet Type(s):</label><br>
								<select data-placeholder="Choose First Type..." id="TypeA" name="TypeA" style="width:295px;" class="chzn-select" tabindex="2" onchange="updateType()" >
								<option value="">...</option>
								<optgroup label="Airbus">
								<option value="A300">A300</option>
								<option value="A310">A310</option>
								<option value="A318/19/20/21">A318/19/20/21</option>     
								<option value="A330">A330</option>     
								<option value="A340">A340</option>     
								<option value="A380">A380</option>
								</optgroup>
								<optgroup label="ATR">
								<option value="ATR42">ATR42</option>
								<option value="ATR72">ATR72</option>
								</optgroup>
								<optgroup label="Boeing">
								<option value="B-717">B-717</option>
								<option value="B-727">B-727</option>
								<option value="B-737">B-737</option>
								<option value="B-757/767">B-757/767</option>
								<option value="B-777">B-777</option>
								<option value="B-787">B-787</option>
								<option value="B-747">B-747</option>
								<option value="DC-9">DC-9</option>
								<option value="DC-10">DC-10</option>
								<option value="MD-11">MD-11</option>
								</optgroup>
								<optgroup label="Bombardier">
								<option value="CRJ-100/200">CRJ-100/200</option>
								<option value="CRJ-700/900/1000">CRJ-700/900/1000</option>
								<option value="BD-700">BD-700</option>
								<option value="CL-300">CL-300</option>
								<option value="CL-600">CL-600</option>
								<option value="CL-601">CL-601</option>
								<option value="CL-604">CL-604</option>
								<option value="CL-605">CL-605</option>
								<option value="DHC-8 100/200/300">DHC-8 100/200/300</option>
								<option value="DHC-8 400">DHC-8 400</option>
								</optgroup>
								<optgroup label="BAe">
								<option value="J31/41">J31/41</option>
								<option value="ATP">ATP</option>
								<option value="AVRO 146 RJ">AVRO 146 RJ</option>
								<option value="HS748">HS748</option>
								</optgroup>
								<optgroup label="Cessna">
								<option value="CE-500">CE-500</option>
								<option value="CE-550">CE-550</option>
								<option value="CE-560">CE-560</option>
								<option value="CE-510">CE-510</option>
								<option value="CE-525">CE-525</option>
								<option value="CE-560XL">CE-560XL</option>
								<option value="CE-650">CE-650</option>
								<option value="CE-680">CE-680</option>
								<option value="CE-750">CE-750</option>
								</optgroup>
								<optgroup label="Dassault Falcon">
								<option value="Falcon 7X">Falcon 7X</option>
								<option value="Falcon 10">Falcon 10</option>
								<option value="Falcon 20">Falcon 20</option>
								<option value="Falcon 50">Falcon 50</option>
								<option value="Falcon 200">Falcon 200</option>
								<option value="Falcon 2000">Falcon 2000</option>
								<option value="Falcon 900">Falcon 900</option>
								</optgroup>
								<optgroup label="Dornier">
								<option value="DO-228">DO-228</option>
								<option value="DO-328">DO-328</option>
								<option value="DO-328">DO-328Jet</option>
								</optgroup>
								<optgroup label="Embraer">
								<option value="EMB-120">EMB-120</option>
								<option value="EMB-Legacy 500">EMB-Legacy 500</option>
								<option value="EMB-135/145/Legacy 600/650">EMB-135/145/Legacy 600/650</option>
								<option value="ERJ 170/190/Lineage 1000">ERJ 170/190/Lineage 1000</option>
								</optgroup>
								<optgroup label="Fokker">
								<option value="F70/100">F70/100</option>
								<option value="F27">F27</option>
								<option value="F28">F28</option>
								</optgroup>
								<optgroup label="Gulfstream">
								<option value="G100">G100</option>
								<option value="G150">G150</option>
								<option value="G200">G200</option>
								<option value="G280">G280</option>
								<option value="GIII">GIII</option>
								<option value="GIV">GIV</option>
								<option value="GV">GV</option>
								<option value="G450">G450</option>
								<option value="G550">G550</option>
								<option value="G650">G650</option>
								</optgroup>
								<optgroup label="Hawker/Beechcraft">
								<option value="B200">B200</option>
								<option value="B300/350">B300/350</option>
								<option value="B1900">B1900</option>
								<option value="4000">4000</option>
								<option value="BAe-125">BAe-125</option>
								<option value="Hawker 750">Hawker 750</option>
								<option value="Hawker 800">Hawker 800</option>
								<option value="Hawker 800XP">Hawker 800XP</option>
								<option value="Hawker 850XP">Hawker 850XP</option>
								<option value="Hawker 900XP">Hawker 900XP</option>
								</optgroup>
								<optgroup label="Israel Aircraft Industries">
								<option value="Westwind">Westwind</option>
								</optgroup>
								<optgroup label="Learjet">
								<option value="LR-20/30 series">LR-20/30 series</option>
								<option value="LR-45">LR-45</option>
								<option value="LR-40">LR-40</option>
								<option value="LR-54/55">LR-54/55</option>
								<option value="LR-60">LR-60</option>
								<option value="LR-70/75">LR-70/75</option>
								<option value="LR-85">LR-85</option>
								</optgroup>
								<optgroup label="Lockheed">
								<option value="L1011">L1011</option>
								</optgroup>
								<optgroup label="SAAB">
								<option value="2000">2000</option>
								<option value="SF340">SF340</option>
								</optgroup>
								<optgroup label="Shorts">
								<option value="330">330</option>
								<option value="360">360</option>
								</optgroup>
								<optgroup label="Sukhoi">
								<option value="Superjet 100">Superjet 100</option>
								</optgroup>
								<optgroup label="Miscellaneous">
								<option value="Military multi engine jet below 70,000 lbs">Military multi engine jet below 70,000 lbs</option>
								<option value="Military multi engine jet above 70,000 lbs">Military multi engine jet above 70,000 lbs</option>
								<option value="Military single engine jet">Military single engine jet</option>
								<option value="Military turboprop">Military turboprop</option>
								<option value="Other civil multi engine jet below 70,000 lbs">Other civil multi engine jet below 70,000 lbs</option>
								<option value="Other civil multi engine jet above 70,000 lbs">Other civil multi engine jet above 70,000 lbs</option>
								</optgroup>
								</select>
							<br>
								<select data-placeholder="Choose Second Type..." id="TypeB" name="TypeB" style="width:295px;" class="chzn-select" tabindex="3" onchange="updateType()">
								<option value="">...</option>
								<optgroup label="Airbus">
								<option value="A300">A300</option>
								<option value="A310">A310</option>
								<option value="A318/19/20/21">A318/19/20/21</option>     
								<option value="A330">A330</option>     
								<option value="A340">A340</option>     
								<option value="A380">A380</option>
								</optgroup>
								<optgroup label="ATR">
								<option value="ATR42">ATR42</option>
								<option value="ATR72">ATR72</option>
								</optgroup>
								<optgroup label="Boeing">
								<option value="B-717">B-717</option>
								<option value="B-727">B-727</option>
								<option value="B-737">B-737</option>
								<option value="B-757/767">B-757/767</option>
								<option value="B-777">B-777</option>
								<option value="B-787">B-787</option>
								<option value="B-747">B-747</option>
								<option value="DC-9">DC-9</option>
								<option value="DC-10">DC-10</option>
								<option value="MD-11">MD-11</option>
								</optgroup>
								<optgroup label="Bombardier">
								<option value="CRJ-100/200">CRJ-100/200</option>
								<option value="CRJ-700/900/1000">CRJ-700/900/1000</option>
								<option value="BD-700">BD-700</option>
								<option value="CL-300">CL-300</option>
								<option value="CL-600">CL-600</option>
								<option value="CL-601">CL-601</option>
								<option value="CL-604">CL-604</option>
								<option value="CL-605">CL-605</option>
								<option value="DHC-8 100/200/300">DHC-8 100/200/300</option>
								<option value="DHC-8 400">DHC-8 400</option>
								</optgroup>
								<optgroup label="BAe">
								<option value="J31/41">J31/41</option>
								<option value="ATP">ATP</option>
								<option value="AVRO 146 RJ">AVRO 146 RJ</option>
								<option value="HS748">HS748</option>
								</optgroup>
								<optgroup label="Cessna">
								<option value="CE-500">CE-500</option>
								<option value="CE-550">CE-550</option>
								<option value="CE-560">CE-560</option>
								<option value="CE-510">CE-510</option>
								<option value="CE-525">CE-525</option>
								<option value="CE-560XL">CE-560XL</option>
								<option value="CE-650">CE-650</option>
								<option value="CE-680">CE-680</option>
								<option value="CE-750">CE-750</option>
								</optgroup>
								<optgroup label="Dassault Falcon">
								<option value="Falcon 7X">Falcon 7X</option>
								<option value="Falcon 10">Falcon 10</option>
								<option value="Falcon 20">Falcon 20</option>
								<option value="Falcon 50">Falcon 50</option>
								<option value="Falcon 200">Falcon 200</option>
								<option value="Falcon 2000">Falcon 2000</option>
								<option value="Falcon 900">Falcon 900</option>
								</optgroup>
								<optgroup label="Dornier">
								<option value="DO-228">DO-228</option>
								<option value="DO-328">DO-328</option>
								<option value="DO-328">DO-328Jet</option>
								</optgroup>
								<optgroup label="Embraer">
								<option value="EMB-120">EMB-120</option>
								<option value="EMB-Legacy 500">EMB-Legacy 500</option>
								<option value="EMB-135/145/Legacy 600/650">EMB-135/145/Legacy 600/650</option>
								<option value="ERJ 170/190/Lineage 1000">ERJ 170/190/Lineage 1000</option>
								</optgroup>
								<optgroup label="Fokker">
								<option value="F70/100">F70/100</option>
								<option value="F27">F27</option>
								<option value="F28">F28</option>
								</optgroup>
								<optgroup label="Gulfstream">
								<option value="G100">G100</option>
								<option value="G150">G150</option>
								<option value="G200">G200</option>
								<option value="G280">G280</option>
								<option value="GIII">GIII</option>
								<option value="GIV">GIV</option>
								<option value="GV">GV</option>
								<option value="G450">G450</option>
								<option value="G550">G550</option>
								<option value="G650">G650</option>
								</optgroup>
								<optgroup label="Hawker/Beechcraft">
								<option value="B200">B200</option>
								<option value="B300/350">B300/350</option>
								<option value="B1900">B1900</option>
								<option value="4000">4000</option>
								<option value="BAe-125">BAe-125</option>
								<option value="Hawker 750">Hawker 750</option>
								<option value="Hawker 800">Hawker 800</option>
								<option value="Hawker 800XP">Hawker 800XP</option>
								<option value="Hawker 850XP">Hawker 850XP</option>
								<option value="Hawker 900XP">Hawker 900XP</option>
								</optgroup>
								<optgroup label="Israel Aircraft Industries">
								<option value="Westwind">Westwind</option>
								</optgroup>
								<optgroup label="Learjet">
								<option value="LR-20/30 series">LR-20/30 series</option>
								<option value="LR-45">LR-45</option>
								<option value="LR-40">LR-40</option>
								<option value="LR-54/55">LR-54/55</option>
								<option value="LR-60">LR-60</option>
								<option value="LR-70/75">LR-70/75</option>
								<option value="LR-85">LR-85</option>
								</optgroup>
								<optgroup label="Lockheed">
								<option value="L1011">L1011</option>
								</optgroup>
								<optgroup label="SAAB">
								<option value="2000">2000</option>
								<option value="SF340">SF340</option>
								</optgroup>
								<optgroup label="Shorts">
								<option value="330">330</option>
								<option value="360">360</option>
								</optgroup>
								<optgroup label="Sukhoi">
								<option value="Superjet 100">Superjet 100</option>
								</optgroup>
								<optgroup label="Miscellaneous">
								<option value="Military multi engine jet below 70,000 lbs">Military multi engine jet below 70,000 lbs</option>
								<option value="Military multi engine jet above 70,000 lbs">Military multi engine jet above 70,000 lbs</option>
								<option value="Military single engine jet">Military single engine jet</option>
								<option value="Military turboprop">Military turboprop</option>
								<option value="Other civil multi engine jet below 70,000 lbs">Other civil multi engine jet below 70,000 lbs</option>
								<option value="Other civil multi engine jet above 70,000 lbs">Other civil multi engine jet above 70,000 lbs</option>
								</optgroup>
								</select>
							<br>							
							</td>
							</tr>
							</table>
							</div>
							<br>
							<div class="box1">
							<table>
							<tr>
								<td colspan="2" class="left" style="vertical-align: top; width: 100%;" >
								<label for="prev_type">Previous Types of Aircraft: </label><br>
								<select data-placeholder="Please select applicable type(s) ..." id="prev_type[]" name="prev_type[]" style="width:295px;" class="chzn-select" multiple tabindex="4">
								<option value="">...</option>
								<optgroup label="Airbus">
								<option value="A300">A300</option>
								<option value="A310">A310</option>
								<option value="A318/19/20/21">A318/19/20/21</option>     
								<option value="A330">A330</option>     
								<option value="A340">A340</option>     
								<option value="A380">A380</option>
								</optgroup>
								<optgroup label="ATR">
								<option value="ATR42">ATR42</option>
								<option value="ATR72">ATR72</option>
								</optgroup>
								<optgroup label="Boeing">
								<option value="B-717">B-717</option>
								<option value="B-727">B-727</option>
								<option value="B-737">B-737</option>
								<option value="B-757/767">B-757/767</option>
								<option value="B-777">B-777</option>
								<option value="B-787">B-787</option>
								<option value="B-747">B-747</option>
								<option value="DC-9">DC-9</option>
								<option value="DC-10">DC-10</option>
								<option value="MD-11">MD-11</option>
								</optgroup>
								<optgroup label="Bombardier">
								<option value="CRJ-100/200">CRJ-100/200</option>
								<option value="CRJ-700/900/1000">CRJ-700/900/1000</option>
								<option value="BD-700">BD-700</option>
								<option value="CL-300">CL-300</option>
								<option value="CL-600">CL-600</option>
								<option value="CL-601">CL-601</option>
								<option value="CL-604">CL-604</option>
								<option value="CL-605">CL-605</option>
								<option value="DHC-8 100/200/300">DHC-8 100/200/300</option>
								<option value="DHC-8 400">DHC-8 400</option>
								</optgroup>
								<optgroup label="BAe">
								<option value="J31/41">J31/41</option>
								<option value="ATP">ATP</option>
								<option value="AVRO 146 RJ">AVRO 146 RJ</option>
								<option value="HS748">HS748</option>
								</optgroup>
								<optgroup label="Cessna">
								<option value="CE-500">CE-500</option>
								<option value="CE-550">CE-550</option>
								<option value="CE-560">CE-560</option>
								<option value="CE-510">CE-510</option>
								<option value="CE-525">CE-525</option>
								<option value="CE-560XL">CE-560XL</option>
								<option value="CE-650">CE-650</option>
								<option value="CE-680">CE-680</option>
								<option value="CE-750">CE-750</option>
								</optgroup>
																<optgroup label="Dassault Falcon">
								<option value="Falcon 7X">Falcon 7X</option>
								<option value="Falcon 10">Falcon 10</option>
								<option value="Falcon 20">Falcon 20</option>
								<option value="Falcon 50">Falcon 50</option>
								<option value="Falcon 200">Falcon 200</option>
								<option value="Falcon 2000">Falcon 2000</option>
								<option value="Falcon 900">Falcon 900</option>
								</optgroup>
								<optgroup label="Dornier">
								<option value="DO-228">DO-228</option>
								<option value="DO-328">DO-328</option>
								<option value="DO-328">DO-328Jet</option>
								</optgroup>
								<optgroup label="Embraer">
								<option value="EMB-120">EMB-120</option>
								<option value="EMB-Legacy 500">EMB-Legacy 500</option>
								<option value="EMB-135/145/Legacy 600/650">EMB-135/145/Legacy 600/650</option>
								<option value="ERJ 170/190/Lineage 1000">ERJ 170/190/Lineage 1000</option>
								</optgroup>
								<optgroup label="Fokker">
								<option value="F70/100">F70/100</option>
								<option value="F27">F27</option>
								<option value="F28">F28</option>
								</optgroup>
								<optgroup label="Gulfstream">
								<option value="G100">G100</option>
								<option value="G150">G150</option>
								<option value="G200">G200</option>
								<option value="G280">G280</option>
								<option value="GIII">GIII</option>
								<option value="GIV">GIV</option>
								<option value="GV">GV</option>
								<option value="G450">G450</option>
								<option value="G550">G550</option>
								<option value="G650">G650</option>
								</optgroup>
								<optgroup label="Hawker/Beechcraft">
								<option value="B200">B200</option>
								<option value="B300/350">B300/350</option>
								<option value="B1900">B1900</option>
								<option value="4000">4000</option>
								<option value="BAe-125">BAe-125</option>
								<option value="Hawker 750">Hawker 750</option>
								<option value="Hawker 800">Hawker 800</option>
								<option value="Hawker 800XP">Hawker 800XP</option>
								<option value="Hawker 850XP">Hawker 850XP</option>
								<option value="Hawker 900XP">Hawker 900XP</option>
								</optgroup>
								<optgroup label="Israel Aircraft Industries">
								<option value="Westwind">Westwind</option>
								</optgroup>
								<optgroup label="Learjet">
								<option value="LR-20/30 series">LR-20/30 series</option>
								<option value="LR-45">LR-45</option>
								<option value="LR-40">LR-40</option>
								<option value="LR-54/55">LR-54/55</option>
								<option value="LR-60">LR-60</option>
								<option value="LR-70/75">LR-70/75</option>
								<option value="LR-85">LR-85</option>
								</optgroup>
								<optgroup label="Lockheed">
								<option value="L1011">L1011</option>
								</optgroup>
								<optgroup label="SAAB">
								<option value="2000">2000</option>
								<option value="SF340">SF340</option>
								</optgroup>
								<optgroup label="Shorts">
								<option value="330">330</option>
								<option value="360">360</option>
								</optgroup>
								<optgroup label="Sukhoi">
								<option value="Superjet 100">Superjet 100</option>
								</optgroup>
								<optgroup label="Miscellaneous">
								<option value="Military multi engine jet below 70,000 lbs">Military multi engine jet below 70,000 lbs</option>
								<option value="Military multi engine jet above 70,000 lbs">Military multi engine jet above 70,000 lbs</option>
								<option value="Military single engine jet">Military single engine jet</option>
								<option value="Military turboprop">Military turboprop</option>
								<option value="Other civil multi engine jet below 70,000 lbs">Other civil multi engine jet below 70,000 lbs</option>
								<option value="Other civil multi engine jet above 70,000 lbs">Other civil multi engine jet above 70,000 lbs</option>
								</optgroup>								
								</select>
								</td>	
							</tr>
							</table>
							</div>
						</td>
						<td style="vertical-align: top; width: 5%;"></td>	
						<td colspan="2" style="vertical-align: top; width: 50%;">
						<div class="box2">
						<label for="totalhr">Aeroplane Flying Hours</label>
						<table>
						<tbody>
						<tr>
							<td class="left" style="vertical-align: top; width: 60%;">
							<label for="totalhr">Total </label>
							</td>
							<td class="right" style="vertical-align: top; width: 40%;">
							<input name="totalhr" type="text"  size="10" />
							</td>
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 60%;">
							<label for="multiengine">Multi Engine</label>
							</td>
							<td class="right" style="vertical-align: top; width: 40%;">
							<input name="multiengine" type="text"  size="10" />
							</td>
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 60%;">
							<label for="multiturbo">Multi Turbojet</label>
							</td>
							<td class="right" style="vertical-align: top; width: 40%;">
							<input name="multiturbo" type="text"  size="10" />
							</td>
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 60%;">
							<label for="pichr">PIC</label>
							</td>
							<td class="right" style="vertical-align: top; width: 40%;">
							<input name="pichr" type="text"  size="10" />
							</td>
						</tr>
						<tr>
							</td>
							<td class="left" style="vertical-align: top; width: 60%;">
							</td>
							<td class="right" style="vertical-align: top; width: 40%;">
							</td>
						</tr>
						</tbody>
						</table>
						</div>
						<br>
						<div class="box2">
						<table>
						<label for="doyouhave">Do you hold (or have you previously held): </label>
						<tr>
							<td class="left" style="vertical-align: top; width: 20%;" >
								<input type="hidden" name="tri" value="No">
								<input type="checkbox" name="tri" value="Yes">TRI
							</td>	
							<td class="left" style="vertical-align: top; width: 20%;" >
								<input type="hidden" name="tre" value="No">
								<input type="checkbox" name="tre" value="Yes">TRE
							</td>
							<td class="left" style="vertical-align: top; width: 20%;" >
								<input type="hidden" name="ire" value="No">
								<input type="checkbox" name="ire" value="Yes">IRE
							</td>
							<td class="left" style="vertical-align: top; width: 40%;" > qualification(s)
							</td>
						</tr>
						</table>
						</td>	
					</tr>
					<tr>
						<td colspan="5" class="left" style="vertical-align: top; width: 100%;">
						</td>
					</tr>
					<tr>
					<td colspan="5" style="vertical-align: top; width: 100%;">
					<div class="box3">
					<label for="haveyoubeen">Are you or have you been a?</label>
					<table width="100%">
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;" >
							<input type="hidden" name="flight_instructor" value="No">
							<input type="checkbox" name="flight_instructor" value="Yes">Flight Instructor/Examiner
							</td>	
							<td class="left" style="vertical-align: top; width: 20%;" >
								<input type="hidden" name="training_captain" value="No">
								<input type="checkbox" name="training_captain" value="Yes">Training Captain
							</td>
							<td class="left" style="vertical-align: top; width: 25%;" >
								<input type="hidden" name="linetraining_captain" value="No">
								<input type="checkbox" name="linetraining_captain" value="Yes">Line Training Captain
							</td>
							<td class="left" style="vertical-align: top; width: 15%;" >
								<input type="hidden" name="line_captain" value="No">
								<input type="checkbox" name="line_captain" value="Yes">Lead Captain
							</td>
						</tr>
						</table>
					</div>	
					</td>
					</tr>
					<tr>
					<td colspan="5" style="vertical-align: top; width: 100%;">
					<div class="box3">
					<label for="haveyouheldmgt">Do you hold or have you held a management position?</label>					
					<table>
						<tr>
							<td class="left" style="vertical-align: top; width: 35%;" >
								<label for="training_mgt">Training</label>
							</td>
							<td class="right" style="vertical-align: top; width: 65%;" >
								<input type="radio" name="training_mgt" value="Yes">Yes
								<input type="radio" name="training_mgt" value="No">No
							</td>	
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 35%;" >
								<label for="operations_mgt">Operations</label>
							</td>
							<td class="right" style="vertical-align: top; width: 65%;" >
								<input type="radio" name="operations_mgt" value="Yes">Yes
								<input type="radio" name="operations_mgt" value="No">No
							</td>
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 35%;" >
								<label for="aircraft_mgt">Aircraft</label>
							</td>
							<td class="right" style="vertical-align: top; width: 65%;" >
								<input type="radio" name="aircraft_mgt" value="Yes">Yes
								<input type="radio" name="aircraft_mgt" value="No">No
							</td>
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 35%;" >
							<label for="brief_mgt">Give a brief description</label>					
							</td>
							<td class="right" style="vertical-align: top; width: 65%;" >
							<textarea wrap="physical" class="defaultTextBrief" id="brief_mgt" name="brief_mgt" ></textarea>
							</td>
						</tr>
					</table>
					</div>
					<br>
					<div class="box3">
					<table width="100%">
					<tr>
						<td colspan="2" class="left" style="vertical-align: top; width: 100%;" >
						<label for="background">Background Disclosure:</label>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="left" style="vertical-align: top; width: 100%;" >
						<label for="incident">Have you ever been involved in an aircraft accident or serious incident? </label>
						</td>
					</tr>
					<tr>
						<td class="right" style="vertical-align: top; width: 35%;" >
							<select name="incident" style="width:100px;" class="chzn-select" tabindex="7">
							<option value="">...</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</td>
						<td class="right" style="vertical-align: top; width: 65%;" >if Yes, please provide details/circumstances
						<textarea wrap="physical" class="defaultTextBrief" id="incident_text" name="incident_text" ></textarea>
						</td>							
					</tr>				
					<tr>
						<td colspan="2" class="left" style="vertical-align: top; width: 100%;" >
						<label for="violation">Have you ever been the subject of an investigation that resulted in a pilot licence violation?  </label>
						</td>
					</tr>
					<tr>
						<td class="right" style="vertical-align: top; width: 35%;" >
							<select name="violation" style="width:100px;" class="chzn-select" tabindex="8">
							<option value="">...</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</td>
						<td class="right" style="vertical-align: top; width: 65%;" >if Yes, please provide details/circumstances
						<textarea wrap="physical" class="defaultTextBrief" id="violation_text" name="violation_text" ></textarea>
						</td>							
					</tr>									
					</table>
					</div>
					<div class="box3">
					<table width="100%">
					<tr>
						<td class="left" style="vertical-align: top; width: 35%;" >
						<label for="misc_mgt">Miscellaneous relevant information including the name of any Metrojet employee willing to act as reference</label>
						</td>
						<td class="right" style="vertical-align: top; width: 65%;" >
						<textarea wrap="physical" class="defaultTextBrief" id="misc_mgt" name="misc_mgt" ></textarea>
						</td>							
					</tr>
					<tr><td><br></td></tr>
					<tr>
						<td class="left" style="vertical-align: top; width: 35%;" >
						<label for="ref_1">Reference 1</label>
						</td>
						<td class="right" style="vertical-align: top; width: 65%;" >
						<table width="100%">
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;">
							<label for="ref_1_name">Name</label></td>
							<td class="left" style="vertical-align: top; width: 70%;" >
							<input name="ref_1_name" type="text"  size="38" /></td>
						</tr>	
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;">
							<label for="ref_1_email">Email</label></td>
							<td class="left" style="vertical-align: top; width: 70%;" >
							<input name="ref_1_email" type="text"  size="38" /></td>
						</tr>
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;">
							<label for="ref_1_address">Address</label></td>
							<td class="left" style="vertical-align: top; width: 70%;" >
							<textarea wrap="physical" class="defaultTextRef" id="ref_1_address" name="ref_1_address" ></textarea></td>
						</tr>	
						</table>
						</td>							
					</tr>
					<tr><td><br></td></tr>
					<tr>
						<td class="left" style="vertical-align: top; width: 35%;" >
						<label for="ref_2">Reference 2</label>
						</td>
						<td class="right" style="vertical-align: top; width: 65%;" >
						<table width="100%">
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;">
							<label for="ref_2_name">Name</label></td>
							<td class="left" style="vertical-align: top; width: 70%;" >
							<input name="ref_2_name" type="text"  size="38" /></td>
						</tr>	
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;">
							<label for="ref_2_email">Email</label></td>
							<td class="left" style="vertical-align: top; width: 70%;" >
							<input name="ref_2_email" type="text"  size="38" /></td>
						</tr>						
						<tr>
							<td class="left" style="vertical-align: top; width: 30%;">
							<label for="ref_2_address">Address</label></td>
							<td class="left" style="vertical-align: top; width: 70%;" >
							<textarea wrap="physical" class="defaultTextRef" id="ref_2_address" name="ref_2_address" ></textarea></td>
						</tr>	
						</table>
						</td>							
					</tr>
					</table>
					</div>
					</td>
					</tr>
					</tbody>
			</table>
			<table align="center" width="700">
				<tr>
					<td colspan="2" class="left" style="vertical-align: top; width: 90%;">
						<label for="cv">Please attach your CV ( PDF file not exceeding 2MB in size) : </label></td>
					<td colspan="2" class="left" style="vertical-align: top; width: 10%;">						
						<input type="file" id="cv" name="cv" width="8" /></td>
				</tr>
				<tr>
					<td colspan="2" class="left" style="vertical-align: top; width: 90%;">
						<label for="cover">Please attach your covering letter ( PDF file not exceeding 2MB in size) : </label></td>
					<td colspan="2" class="left" style="vertical-align: top; width: 10%;">						
						<input type="file" id="cover" name="cover" width="8" /></td>
				</tr>				
				<tr>
					<td colspan="2" class="left" style="vertical-align: top; width: 90%;">
						<label for="cv">Please attach your Passport photo (JPEG file resolution 600x600 or above) : </label></td>
					<td colspan="2" class="left" style="vertical-align: top; width: 10%;">		
						<input type="file" id="photo" name="photo" width="8" /></td>
				</tr>			
				<tr>
					<td colspan="2" class="left" style="vertical-align: top; width: 90%;">
						<label for="cv">Please attach your Full Length photo (JPEG file not exceeding 2MB in size) : </label></td>
					<td colspan="2" class="left" style="vertical-align: top; width: 10%;">		
						<input type="file" id="photo_f" name="photo_f" width="8" /></td>
				</tr>			
			</table>
			<br>
			<table align="center" width="700">
			<tbody>
			<tr>
				<td colspan="6" style="vertical-align: top; width: 100%;">
				<h1>PILOT FLYING TIME SUMMARY</h1>
				</td>
			</tr>
			<tr>
				<td >
				<label>Name</label>
				</td>
				<td >
				<input readonly type="text" id="fullname" name="fullname" size="40" value="" />
				</td>
				<td >
				</td>
				<td >
				<label>Current Multi Jet Type(s)</label>
				</td>
				<td >
				<input type="text" id="TypeA_t" name="TypeA_t" size="8" value="" readonly />
				</td>
				<td >
				<input type="text" id="TypeB_t" name="TypeB_t" size="8" value="" readonly />
				</td>
			</tr>
			<tr>
			<td colspan="6" align="right"><font size="2" >*Please fill in all&nbsp;</font><font style="background-color: #CCCCFF;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font size="2" >boxes</font><br>
			<font size="2" >**Please input <b>0</b> for no value field(s): do not leave blank</font>
			</td>
			</tr>
			</tbody>
			</table>
			<br>
			<table align="center" width="710">
			<tbody>
			<tr>
				<td style="vertical-align: top; width: 100%;">
				<table border="1">
				<tbody>
				<tr>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>Aircraft Type</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>P1 / PIC</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>P2 / SIC</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>Dual (Excl<br> Sim.)</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>Simulator</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>Instructor <br> A/C - Sim</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>Instrument <br> Flying</b></font>
					</td>
					<td bgcolor="#003366" align="center">
					<font color="white" size="2" ><b>Total</b></font>
					</td>
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Current FW Multi Jet <input readonly type="text" id="c6" name="c6" value="" size="3" /></font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" id="d6" name="d6" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" id="e6" name="e6" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" id="f6" name="f6" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g6" name="g6" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h6" name="h6" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i6" name="i6" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly id="j6" name="j6" size="5" />
					</td>
				</tr>	
				<tr>
					<td align="left">
					<label><font size="2">Current FW Multi Jet <input readonly type="text" id="c7" name="c7" value="" size="3" /></font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d7" name="d7" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e7" name="e7" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f7" name="f7" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g7" name="g7" value="" size="5"  onchange="updatesum()"/>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h7" name="h7" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i7" name="i7" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j7" name="j7" value="" size="5" /><b></b>
					</td>
				</tr>				
				<tr>
					<td align="left">
					<label><font size="2">Other Fixed Wing Multi Jet</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d8" name="d8" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e8" name="e8" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f8" name="f8" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g8" name="g8" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h8" name="h8" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i8" name="i8" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j8" name="j8" value="" size="5" /><b></b>
					</td>
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Fixed Wing Multi Turbo-prop</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d9" name="d9" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e9" name="e9" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f9" name="f9" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g9" name="g9" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h9" name="h9" value="" size="6"  onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i9" name="i9" value="" size="8"  onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j9" name="j9" value="" size="5" /><b></b>
					</td>					
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Fixed Wing Multi Eng Piston</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d10" name="d10" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e10" name="e10" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f10" name="f10" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g10" name="g10" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h10" name="h10" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i10" name="i10" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j10" name="j10" value="" size="5" />
					</td>
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Fixed Wing SE Piston</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d11" name="d11" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e11" name="e11" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f11" name="f11" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g11" name="g11" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h11" name="h11" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i11" name="i11" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j11" name="j11" value="" size="5" /><b></b>
					</td>	
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Fixed Wing SE Turbo-Prop</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d12" name="d12" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e12" name="e12" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f12" name="f12" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g12" name="g12" value="" size="5"  onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h12" name="h12" value="" size="6"  onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i12" name="i12" value="" size="8"  onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j12" name="j12" value="" size="5" /><b></b>
					</td>
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Fixed Wing SE Jet</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d13" name="d13" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e13" name="e13" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f13" name="f13" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g13" name="g13" value="" size="5" onchange="updatesum()"/>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h13" name="h13" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i13" name="i13" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j13" name="j13" value="" size="5" /><b></b>
					</td>
				</tr>
				<tr>
					<td align="left">
					<label><font size="2">Other Types</font></label>
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="d14" name="d14" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="e14" name="e14" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="f14" name="f14" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="g14" name="g14" value="" size="5" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="h14" name="h14" value="" size="6" onchange="updatesum()" />
					</td>
					<td bgcolor="CCCCFF" align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="i14" name="i14" value="" size="8" onchange="updatesum()" />
					</td>
					<td align="left">
					<input readonly type="text"  id="j14" name="j14" value="" size="5" /><b></b>
					</td>
				</tr>
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Fixed Wing Multi Jet</font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d15" name="d15" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e15" name="e15" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f15" name="f15" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g15" name="g15" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h15" name="h15" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i15" name="i15" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j15" name="j15" value="" size="5" /><b></b>
					</td>					
				</tr>
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Fixed Wing Turbo-Prop</font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d16" name="d16" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e16" name="e16" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f16" name="f16" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g16" name="g16" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h16" name="h16" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i16" name="i16" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j16" name="j16" value="" size="5" /><b></b>
					</td>					
				</tr>				
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Fixed Wing Multi-Eng</font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d17" name="d17" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e17" name="e17" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f17" name="f17" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g17" name="g17" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h17" name="h17" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i17" name="i17" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j17" name="j17" value="" size="5" /><b></b>
					</td>					
				</tr>	
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Fixed Wing SE</font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d18" name="d18" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e18" name="e18" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f18" name="f18" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g18" name="g18" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h18" name="h18" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i18" name="i18" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j18" name="j18" value="" size="5" /><b></b>
					</td>					
				</tr>	
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Fixed Wing </font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d19" name="d19" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e19" name="e19" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f19" name="f19" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g19" name="g19" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h19" name="h19" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i19" name="i19" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j19" name="j19" value="" size="5" /><b></b>
					</td>					
				</tr>	
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Other Category </font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d20" name="d20" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e20" name="e20" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f20" name="f20" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g20" name="g20" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h20" name="h20" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i20" name="i20" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j20" name="j20" value="" size="5" /><b></b>
					</td>					
				</tr>	
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>GRAND TOTAL - All Aircraft </font></b></label>
					</td>
					<td align="left">
					<input readonly type="text"  id="d21" name="d21" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="e21" name="e21" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="f21" name="f21" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="g21" name="g21" value="" size="5" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="h21" name="h21" value="" size="6" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="i21" name="i21" value="" size="8" /><b></b>
					</td>
					<td align="left">
					<input readonly type="text"  id="j21" name="j21" value="" size="5" /><b></b>
					</td>					
				</tr>					
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Last 3 months </font></b></label>
					</td>
					<td colspan="6" align="left">
					&nbsp;
					</td>
					<td align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="total_3m" name="total_3m" value="" size="5" />
					</td>					
				</tr>					
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Last 6 months </font></b></label>
					</td>
					<td colspan="6" align="left">
					&nbsp;
					</td>
					<td align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="total_6m" name="total_6m" value="" size="5" />
					</td>					
				</tr>					
				<tr>
					<td bgcolor="#003366" align="left">
					<label><font color="white" size="2" ><b>Total - Last 12 months </font></b></label>
					</td>
					<td colspan="6" align="left">
					&nbsp;
					</td>
					<td align="left">
					<input style="background-color: #CCCCFF;" type="text"  id="total_12m" name="total_12m" value="" size="5" />
					</td>					
				</tr>					
			</tr>
			</tbody>
			</table>
				</td>
			</tr>
			</tbody>
			</table>
			<br>
			<table align="center" width="700">
			<tbody>
				<tr>
					<td class="left" style="vertical-align: top; width: 35%;" >
					<label for="pfts_comments">Comments:</label>					
					</td>
					<td class="right" style="vertical-align: top; width: 65%;" >
					<textarea wrap="physical" class="defaultTextBrief" id="pfts_comments" name="pfts_comments" ></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="left" style="vertical-align: top; width: 100%;" >
					<p align="justify"><input type="checkbox" id="declare" name="declare" value="Yes">&nbsp;&nbsp;I hereby declare that, to the best of my knowledge and belief, the particulars and answers contained in this questionnaire are true, complete and correct.</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="left" style="vertical-align: top; width: 100%;" >
					<p align="justify"><input type="checkbox" id="pics" name="pics" value="Yes">&nbsp;&nbsp;<b>Personal Information Collection Statement </b><br>
Personal data provided by applicants will be used by Metrojet to assess the applicant&#39;s suitability for the position for which they have applied or, in the case of an unsolicited expression of interest, for any other suitable vacancy.<br><br>
We will retain the personal data of applicants for a period of 12 months.  For applicants who are former employees of Metrojet, we may retrieve any available employment-related records for reference.<br><br>
Under the Personal Data (Privacy) Ordinance, any applicant has the right to request access to, and request correction of, personal data in relation to his/her application. Any applicant who wishes to exercise these rights should use the &#39;Contact Us&#39; facility to request a "Data Access Request Form" which should be completed and forwarded to our HR Department.  

</p>
					</td>
				</tr>
			</tbody>
			</table>
			<table class="submission" align="center">
				<tbody>
					<tr>
						<td colspan="2">
							<div class="button">
								<input type="submit" name="Submit" value="Submit" /></div>
						</td>
					</tr>
				</tbody>
			</table>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
	<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
	<script type="text/javascript"> 
    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
	</script>
		</form>
	</div>
</section>

<footer>
	<center>
		<p><a href="mailto:pilotjobquery@metrojet.com">Contact Us</a> | 
		Copyright &copy; 2013 Metrojet Limited All rights reserved</p>
	</center>
</footer>

</body>
</html>