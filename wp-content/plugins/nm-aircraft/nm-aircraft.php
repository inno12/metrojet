<?php
/*
	Plugin Name: Aircraft Post
	Plugin URI: 
	Description: Aircraft Post Plugin
	Version: 1.0
	Author: Neshmedia-BD
	Author URI: http://www.neshmedia-bd.com 
*/	

include_once( 'nm-aircraft-shortcode.php');

// add action to create aircraft post type
add_action( 'init', 'aircraft_post_create' );
if( !function_exists('aircraft_post_create') ){
	function aircraft_post_create() {
		
		register_post_type( 'aircraft',
			array(
				'labels' => array(
					'name'               => __('Aircrafts', 'nm-aircraft'),
					'singular_name'      => __('Aircraft', 'nm-aircraft'),
					'add_new'            => __('Add New', 'nm-aircraft'),
					'add_new_item'       => __('Add New Aircraft', 'nm-aircraft'),
					'edit_item'          => __('Edit Aircraft', 'nm-aircraft'),
					'new_item'           => __('New Aircraft', 'nm-aircraft'),
					'all_items'          => __('All Aircrafts', 'nm-aircraft'),
					'view_item'          => __('View Aircraft', 'nm-aircraft'),
					'search_items'       => __('Search Aircraft', 'nm-aircraft'),
					'not_found'          => __('No aircrafts found', 'nm-aircraft'),
					'not_found_in_trash' => __('No aircrafts found in Trash', 'nm-aircraft'),
					'parent_item_colon'  => '',
					'menu_name'          => __('Aircrafts', 'nm-aircraft')
				),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'aircraft'  ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				'supports'           => array( 'title', 'thumbnail' )
			)
		);
		
		register_taxonomy(
			'aircraft_type', array("aircraft"), array(
				'show_in_quick_edit'         => true,
    			'meta_box_cb'                => false,
				'hierarchical' => true,
				'show_admin_column' => true,
				'label' => __('Aircraft Types', 'nm-aircraft'), 
				'singular_label' => __('Aircraft Type', 'nm-aircraft'), 
				'rewrite' => array( 'slug' => 'aircraft_type' )));
		register_taxonomy_for_object_type('aircraft_type', 'aircraft');
		
		
		$labels = array(
			'name'              => _x( 'Registration', 'aircraft_registration general name', 'nm-aircraft' ),
			'singular_name'     => _x( 'Aircraft Registration', 'aircraft_registration singular name', 'nm-aircraft' ),
			'search_items'      => __( 'Search Aircraft Registration', 'nm-aircraft' ),
			'all_items'         => __( 'All Aircraft Registration', 'nm-aircraft' ),
			'parent_item'       => __( 'Parent Aircraft Registration', 'nm-aircraft' ),
			'parent_item_colon' => __( 'Parent Aircraft Registration:', 'nm-aircraft' ),
			'edit_item'         => __( 'Edit Aircraft Registration', 'nm-aircraft' ),
			'update_item'       => __( 'Update Aircraft Registration', 'nm-aircraft' ),
			'add_new_item'      => __( 'Add New Aircraft Registration', 'nm-aircraft' ),
			'new_item_name'     => __( 'New Aircraft Registration', 'nm-aircraft' ),
			'menu_name'         => __( 'Registration', 'nm-aircraft' ),
		);
		
		register_taxonomy(
			'aircraft_registration', array('aircraft'), array(
				'show_in_quick_edit'         => false,
    			'meta_box_cb'                => false,
				'hierarchical' => true, 
				'show_admin_column' => false,
				//'label' => __('Registration', 'nm-aircraft'),
				'labels' => $labels, 
				'singular_label' => __('Aircraft Registration', 'nm-aircraft'),  
				'rewrite' => array( 'slug' => 'registration'  )));
		register_taxonomy_for_object_type('aircraft_registration', 'aircraft');	
		
		register_post_type( 'aircraft_location',
			array(
				'labels' => array(
					'name'               => __('Locations', 'nm-aircraft'),
					'singular_name'      => __('MRO Location', 'nm-aircraft'),
					'add_new'            => __('Add New', 'nm-aircraft'),
					'add_new_item'       => __('Add New Location', 'nm-aircraft'),
					'edit_item'          => __('Edit Location', 'nm-aircraft'),
					'new_item'           => __('New Location', 'nm-aircraft'),
					'all_items'          => __('Locations', 'nm-aircraft'),
					'view_item'          => __('View Location', 'nm-aircraft'),
					'search_items'       => __('Search Location', 'nm-aircraft'),
					'not_found'          => __('No locations found', 'nm-aircraft'),
					'not_found_in_trash' => __('No locations found in Trash', 'nm-aircraft'),
					'parent_item_colon'  => '',
					'menu_name'          => __('Location', 'nm-aircraft')
				),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'location' ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 1,
				'show_in_menu' 		 => 'edit.php?post_type=aircraft',
				'supports'           => array( 'title', 'editor', 'thumbnail' )
			)
		);
		
		
		register_taxonomy(
			'maintenance_type', array('aircraft_location'), array(
				'hierarchical' => true, 
				'show_admin_column' => true,
				'label' => __('Maintenance Type', 'nm-aircraft'), 
				'singular_label' => __('Maintenance Type', 'nm-aircraft'),  
				'rewrite' => array( 'slug' => 'maintenance_type'  )));
		register_taxonomy_for_object_type('maintenance_type', 'aircraft_location');
	}
	
	add_filter('single_template', 'aircraft_location_template');
	add_action('template_redirect', 'aircraft_type_template');
}

if( !function_exists('aircraft_location_template') ){
	function aircraft_location_template($single_template) {
		global $post;

		if ($post->post_type == 'aircraft_location') {
			$single_template = dirname( __FILE__ ) . '/single-location.php';
		}
		return $single_template;	
	}
}

if( !function_exists('aircraft_type_template') ){
	function aircraft_type_template() {
		if(is_archive() && is_tax('aircraft_type')){
			$category = get_queried_object();
			if($category->taxonomy && $category->taxonomy=='aircraft_type'){
				include( dirname( __FILE__ ) . '/taxonomy-aircraft_type.php');
				exit;
			}
		}
	}
}

function mro_location_registration_maintenance_add_meta_box() {
	add_meta_box(
		'mro_location_registration_maintenance-mro-location-registration-maintenance',
		__( 'MRO Location & Registration & Maintenance', 'mro_location_registration_maintenance' ),
		'mro_location_registration_maintenance_html',
		'aircraft',
		'normal',
		'default'
	);
}
add_action( 'add_meta_boxes', 'mro_location_registration_maintenance_add_meta_box' );

function mro_location_registration_maintenance_html( $post) {
	wp_nonce_field( '_mro_location_registration_maintenance_nonce', 'mro_location_registration_maintenance_nonce' ); 
	
	$aircraft_registrations = get_terms('aircraft_registration', array('parent' => 0, 'hide_empty' => 0, 'orderby'=>"menu_order", 'order' => 'ASC'));
	$maintenance_types = get_terms('maintenance_type', array('parent' => 0, 'hide_empty' => 0, 'orderby'=>"menu_order", 'order' => 'ASC'));

	$mro_locations = get_post_meta($post->ID,'mro_locations',true);
	//echo '<pre>';print_r($mro_locations);echo '</pre>';
	$location_args = array(
		'posts_per_page' => -1,
		'post_type' => 'aircraft_location',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	
	$locations = new WP_Query($location_args);
	if( $locations->have_posts() ){
		while($locations->have_posts()){
			$locations->the_post();
			$location = get_post();
			
			$loc_checked = (isset($mro_locations[$location->ID]) && in_array($location->ID,$mro_locations[$location->ID]['loc'])) ? ' checked="checked"' : '';
			?>
            <div class="air-loc-content">
            <label><input type="checkbox" name="mro_locations[<?php echo $location->ID;?>][loc][]" id="mro_location_<?php echo $location->ID;?>" value="<?php echo $location->ID;?>" <?php echo $loc_checked;?>><?php echo get_the_title($location->ID); ?></label>
            <?php
			if(count($aircraft_registrations)){
				echo '<ul class="af_reg_list">';
				foreach($aircraft_registrations as $aircraft_registration){
					$reg_checked = (isset($mro_locations[$location->ID]) && isset($mro_locations[$location->ID]['reg'][$aircraft_registration->term_id]) && in_array($aircraft_registration->term_id,$mro_locations[$location->ID]['reg'][$aircraft_registration->term_id]['reg'])) ? ' checked="checked"' : '';
					
					echo '<li>';
						echo '<label><input type="checkbox" name="mro_locations['.$location->ID.'][reg]['.$aircraft_registration->term_id.'][reg][]" value="'.$aircraft_registration->term_id.'" '.$reg_checked.'>'.$aircraft_registration->name.'</label>';
						if(count($maintenance_types)){
							echo '<ul class="af_mn_type_list">';
							foreach($maintenance_types as $maintenance_type){
								$mn_checked = (isset($mro_locations[$location->ID]) && isset($mro_locations[$location->ID]['reg'][$aircraft_registration->term_id]) && isset($mro_locations[$location->ID]['reg'][$aircraft_registration->term_id]['type']) && in_array($maintenance_type->term_id,$mro_locations[$location->ID]['reg'][$aircraft_registration->term_id]['type'])) ? ' checked="checked"' : '';
								echo '<li>';
									echo '<label><input type="checkbox" name="mro_locations['.$location->ID.'][reg]['.$aircraft_registration->term_id.'][type][]" value="'.$maintenance_type->term_id.'" '.$mn_checked.'>'.$maintenance_type->name.'</label>';
							}
							echo '</ul>';
						}
					echo '</li>';
				}
				echo '</ul>';
			}
			?>
            </div>
        	<?php
		}
	}
	wp_reset_query();
	?>
    <style type="text/css">
	.air-loc-content{
		display:inline-block;
		width:20%;
		margin-right:2%;
	}
	.af_reg_list{
		margin-top:0;
		padding-left:15px;
	}
	.af_mn_type_list{
		padding-left:15px;
	}
	</style>
    <?php
}

function mro_location_registration_maintenance_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['mro_location_registration_maintenance_nonce'] ) || ! wp_verify_nonce( $_POST['mro_location_registration_maintenance_nonce'], '_mro_location_registration_maintenance_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['mro_locations'] ) ){
		update_post_meta( $post_id, 'mro_locations', $_POST['mro_locations'] );
	}
	else{
		update_post_meta( $post_id, 'mro_locations', array() );
	}
}
add_action( 'save_post', 'mro_location_registration_maintenance_save' );



add_filter('manage_edit-aircraft_columns', 'aircraft_columns_head');
add_action('manage_posts_custom_column', 'aircraft_columns_content', 10, 2);

if( !function_exists('aircraft_columns_head') ){
	function aircraft_columns_head($defaults) {
		$new_column = array();
		foreach($defaults as $column_key=>$default){
			$new_column[$column_key] = $default;
			if($default=='Aircraft Types'){
				$new_column['mro_location'] = 'MRO Location';
				$new_column['mro_registration'] = 'Registration';
			}
		}
		return $new_column;
	}
}

if( !function_exists('aircraft_columns_content') ){
	function aircraft_columns_content($column_name, $post_ID) {
		if ($column_name == 'mro_location') {
			$mro_locations = get_aircraft_mro_location($post_ID);
			if ($mro_locations) {
				echo implode(', ',$mro_locations);
			}
		}
		else if ($column_name == 'mro_registration') {
			$registrations = get_aircraft_registration($post_ID);
			if ($registrations) {
				echo implode(', ',$registrations);
			}
		}
	}
}

if( !function_exists('get_aircraft_mro_location') ){
	function get_aircraft_mro_location($post_id) {
		$mro_locations = get_post_meta($post_id,'mro_locations',true);
		$locations = array();
		if(is_array($mro_locations) && count($mro_locations)){
			foreach($mro_locations as $location_id=>$mro_location){
				$locations[$location_id] = get_the_title($location_id);
			}
		};
		
		return $locations;
	}
}

if( !function_exists('get_aircraft_registration') ){
	function get_aircraft_registration($post_id) {
		$mro_locations = get_post_meta($post_id,'mro_locations',true);
		$registrations = array();
		if(is_array($mro_locations) && count($mro_locations)){
			foreach($mro_locations as $location_id=>$mro_location){
				if(isset($mro_location['reg']) && count($mro_location['reg'])){
					foreach($mro_location['reg'] as $reg_id=>$reg){
						$term = get_term( $reg_id, 'aircraft_registration' );
						if($term->term_id)
							$registrations[$term->term_id] = $term->name;
					}
				}
			}
		};
		
		return $registrations;
	}
}

if ( is_admin() ) {
	// registration category
	add_action( 'aircraft_registration_add_form_fields', 'aircraft_registration_add_form_fields' );
	add_action( 'aircraft_registration_edit_form_fields', 'aircraft_registration_edit_form_fields', 10, 2 );
	add_action( 'created_term', 'aircraft_registration_created_term', 10, 3 );
	add_action( 'edit_term', 'aircraft_registration_edit_term', 10, 3 );
	add_filter( 'manage_edit-aircraft_registration_columns', 'aircraft_registration_columns' );
	add_filter( 'manage_aircraft_registration_custom_column', 'aircraft_registration_column', 10, 3 );
}

if( !function_exists('aircraft_registration_add_form_fields') ){
	function aircraft_registration_add_form_fields() {
		echo '<div class="form-field">';
		echo '<label for="maintenance_type_1">';
		echo __( 'Maintenance Type', 'nm-aircraft' );
		echo '</label>';
		echo ' ';
		echo '<label><input type="checkbox" id="maintenance_type_1" name="_maintenance_type[]" class="postform" value="Line" autocomplete="off"/>Line</label>';
		echo '<label><input type="checkbox" id="maintenance_type_2" name="_maintenance_type[]" class="postform" value="Base" autocomplete="off"/>Base</label>';
		echo '</div>';
	}
}

if( !function_exists('aircraft_registration_edit_form_fields') ){	
	function aircraft_registration_edit_form_fields( $term, $taxonomy ) {
		$maintenance_type = explode(',',get_term_meta( $term->term_id, '_maintenance_type', true ));
		echo '<tr class="form-field">';
		echo '<th scope="row" valign="top">';
		echo '<label>';
		echo __( 'Maintenance Type', 'nm-aircraft' );
		echo '</label>';
		echo '</th>';
		echo '<td>';
		echo '<label><input type="checkbox" id="maintenance_type_1" name="_maintenance_type[]" class="postform" value="Line" '.(in_array('Line',$maintenance_type)?' checked="checked"':'').' autocomplete="off"/>Line</label>';
		echo ' <label><input type="checkbox" id="maintenance_type_2" name="_maintenance_type[]" class="postform" value="Base" '.(in_array('Base',$maintenance_type)?' checked="checked"':'').' autocomplete="off"/>Base</label>';
		echo '</td>';
		echo '</tr>';
	}
}

if( !function_exists('aircraft_registration_created_term') ){
	function aircraft_registration_created_term( $term_id, $term_taxonomy_id, $taxonomy ) {
		aircraft_registration_edit_term( $term_id, $term_taxonomy_id, $taxonomy );
	}
}

if( !function_exists('aircraft_registration_edit_term') ){
	function aircraft_registration_edit_term( $term_id, $term_taxonomy_id, $taxonomy ) {
		$maintenance_type = isset( $_POST['_maintenance_type'] ) ? $_POST['_maintenance_type'] : array();
		if ( count( $maintenance_type ) > 0 ) {
			update_term_meta( $term_id, '_maintenance_type', implode(',',$maintenance_type) );
		} else {
			delete_term_meta( $term_id, '_maintenance_type' );
		}
	}
}

if( !function_exists('aircraft_registration_columns') ){
	function aircraft_registration_columns( $columns ) {
		$columns['maintenance_type'] = __( 'Maintenance Type', 'nm-aircraft' );
		return $columns;
	}
}

if( !function_exists('aircraft_registration_column') ){
	function aircraft_registration_column( $columns, $column, $term_id ) {
		if ( $column == 'maintenance_type' ) {
			$maintenance_type = get_term_meta( $term_id, '_maintenance_type', true );
			
			$columns .= $maintenance_type;
		}
		return $columns;
	}
}

function nm_filter_plugin_updates( $value ) {
   if( isset( $value->response['advanced-custom-fields-pro/acf.php'] ) ) {        
      unset( $value->response['advanced-custom-fields-pro/acf.php'] );
    }
    return $value;
}
add_filter( 'site_transient_update_plugins', 'nm_filter_plugin_updates' );

if( !function_exists('get_aircraft_filter_model') ){
	function get_aircraft_filter_model(){
		global $wpdb;
		icl_register_string( 'NM Aitcraft', 'all', 'ALL');
		$text_all = icl_translate('NM Aitcraft', 'all', 'ALL');
		$filter_model = array();
		$aircraft_types = get_terms('aircraft_type', array('parent' => 0, 'hide_empty' => 0, 'orderby'=>"menu_order", 'order'      => 'ASC'));
		foreach($aircraft_types as $aircraft_type){
			$filter_model[] = array(
				'class' => 'category',
				'id'	=> $aircraft_type->term_id,
				'title'	=> $aircraft_type->name.' '.$text_all,
				'link'	=> get_term_link( $aircraft_type, 'aircraft_type' )
			);
			
			$aircraft_args = array(
				'posts_per_page' => -1,
				'tax_query' => array(
				'relation' => 'AND',
					array(
						'taxonomy' => 'aircraft_type',
						'field' => 'id',
						'terms' => $aircraft_type->term_id,
						'operator' => 'IN'
					)
				),
				'post_type' => 'aircraft',
				'orderby' => 'menu_order',
				'order' => 'ASC'
			);
			
			$aircrafts = new WP_Query($aircraft_args);
			if( $aircrafts->have_posts() ){
				while($aircrafts->have_posts()){
					$aircrafts->the_post();
					$aircraft = get_post();
					
					$filter_model[] = array(
						'class' => 'post',
						'id'	=> $aircraft->ID,
						'title'	=> get_the_title($aircraft->ID),
						'link'	=> get_post_permalink($aircraft->ID)
					);
				}
			}
			wp_reset_query();
		}
		
		$location_args = array(
			'posts_per_page' => -1,
			'post_type' => 'aircraft_location',
			'orderby' => 'menu_order'
		);
		
		$locations = new WP_Query($location_args);
		if( $locations->have_posts() ){
			while($locations->have_posts()){
				$locations->the_post();
				$location = get_post();
				
				$filter_model[] = array(
					'class' => 'location',
					'id'	=> $location->ID,
					'title'	=> get_the_title($location->ID),
					'link'	=> get_post_permalink($location->ID)
				);
			}
		}
		wp_reset_query();
		
		return $filter_model;
	}
}

add_action("wp_ajax_nopriv_get_aircraft_data", "get_aircraft_data");
add_action("wp_ajax_get_aircraft_data", "get_aircraft_data");

if( !function_exists('get_aircraft_data') ){
	function get_aircraft_data(){
		global $wpdb;
		$json = array();
		if(isset($_POST['model_id']) && $_POST['model_id']){
			$model_id = (int)$_POST['model_id'];
			$model_type = $_POST['model_type'];
			
			$language_code = ICL_LANGUAGE_CODE;
			
			$base_maintenance = icl_translate('NM Aitcraft', 'base-maintenance', 'Base maintenance');
			$line_maintenance = icl_translate('NM Aitcraft', 'line-maintenance', 'Line maintenance');
		
			if($model_type=='aircraft_type'){
				$aircraft_id = $model_id;
				$aircraft = get_post($aircraft_id);
				$mro_locations = get_post_meta($aircraft_id,'mro_locations',true);
				
				$new_mro_locations = array();
				if(is_array($mro_locations) && count($mro_locations)){
					foreach($mro_locations as $location_id=>$mro_location){
						$original_ID = icl_object_id($location_id, 'any', false, $language_code );
						$sort_order = get_post_field( 'menu_order', $original_ID);
						$new_mro_locations[$location_id] = $sort_order;
						
						$new_reg = array();
						foreach($mro_location['reg'] as $reg_id=>$reg){
							$regInfo = get_term( $reg_id, 'aircraft_registration' );
							$new_reg[$reg_id] = $regInfo->term_order;
						}
						asort($new_reg);
						$reg_clone = array();
						foreach($new_reg as $key=>$order){
							$reg_clone[$key] = $mro_location['reg'][$key];
						}
						$mro_locations[$location_id]['reg'] = $reg_clone;
					}
					asort($new_mro_locations);
					//array_multisort($new_mro_locations, SORT_ASC, $mro_locations);
					
					$mro_locations_clone = array();
					foreach($new_mro_locations as $key=>$order){
						$mro_locations_clone[$key] = $mro_locations[$key];
					}
					$mro_locations = $mro_locations_clone;
				}
													
				$html = '';
				
				$html .= '<div class="aircraft_info">';
					$html .= '<div class="aircraft_info_top"><span class="mn_type_base">'.$base_maintenance.'</span><span class="mn_type_line">'.$line_maintenance.'</span></div>';
					$html .= '<h3 class="aircraft_title">'.get_the_title($aircraft_id).'</h3>';
					$html .= '<div class="aircraft_reg_info limoking-item limoking-accordion-item style-1">';
					if(is_array($mro_locations) && count($mro_locations)){
						$i=1;
						foreach($mro_locations as $location_id=>$mro_location){
																$ac_class = ($i==1)?'':'';
																$icon_class = 'icon-plus';
																//$ac_class = ($i==1)?' pre-active active':'';
																//$icon_class = ($i==1)?'icon-minus':'icon-plus';
							$html .= '<div class="accordion-tab '.$ac_class.'">';
								$original_ID = icl_object_id($location_id, 'any', false, $language_code );
								$html .= '<h4 class="accordion-title"><i class="'.$icon_class.'"></i><span>'.get_the_title($original_ID).'</span></h4>';
								$html .= '<div class="accordion-content">';
									$html .= '<ul class="airctaft_loc_reg">';
									if(isset($mro_location['reg']) && count($mro_location['reg'])){
										foreach($mro_location['reg'] as $reg_id=>$reg){
											$regInfo = get_term( $reg_id, 'aircraft_registration' );
											if($regInfo->term_id){
												$base_class = '';
												$line_class = '';
												if(isset($reg['type']) && count($reg['type'])){
													foreach($reg['type'] as $reg_type){
														$typeInfo = get_term( $reg_type, 'maintenance_type' );
														if($typeInfo->slug && strpos($typeInfo->slug,'base')===0)
															$base_class = 'maintenance_base';
														if($typeInfo->slug && strpos($typeInfo->slug,'line')===0)
															$line_class = 'maintenance_line';
													}
												}
												$html .= '<li><span class="reg_icon"><i class="'.$base_class.'"></i><i class="'.$line_class.'"></i></span><span class="reg_name">'.$regInfo->name.'</span></li>';
											}
										}
									}
									$html .= '</ul>';
								$html .= '</div>';
							$html .= '</div>';
							$i++;
						}
					}
					$html .= '</div>';
				$html .= '</div>';
			}
			$json['success'] = 1;
			$json['html'] = $html;
		}
		echo json_encode($json);
		exit();
	}
}

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('post','page'));
    }

	return $query;
}

add_filter('pre_get_posts','searchfilter');

function nm_sortByOrder($a, $b) {
    return $a['order'] - $b['order'];
}
?>