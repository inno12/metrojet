<?php 
	get_header('aircraft'); 
	
	global $limoking_contact_car, $wp_query;
	$limoking_contact_car = clone $wp_query;
	
	wp_enqueue_style('aircraft_main_css', plugins_url() . '/nm-aircraft/css/style.css', false,'1.0');
	
	while( have_posts() ){ the_post();
	$_locationID = get_the_ID();
?>
<div class="limoking-content aircraft-location-content">

	<?php 
		global $limoking_sidebar, $theme_option, $limoking_post_option, $limoking_is_ajax;
		
		
		$limoking_sidebar = array(
			'type'=> 'no-sidebar',
			'left-sidebar'=> '', 
			'right-sidebar'=> ''
		); 				
		//$limoking_sidebar = limoking_get_sidebar_class($limoking_sidebar);
		
		$language_code = ICL_LANGUAGE_CODE;
		
		icl_register_string( 'NM Aitcraft', 'mro-location-facilities', 'MRO Certifications and Facilities');
		icl_register_string( 'NM Aitcraft', 'mro', ' MRO');
		icl_register_string( 'NM Aitcraft', 'aircraft-maintenance-capibality', 'Aircraft Maintenance Capability');
		icl_register_string( 'NM Aitcraft', 'aircraft-ratings', 'Aircraft Ratings');
		icl_register_string( 'NM Aitcraft', 'mro-capability', 'MRO Capability');
		icl_register_string( 'NM Aitcraft', 'mro-facility', 'MRO Facility');
		
		$base_maintenance = icl_translate('NM Aitcraft', 'base-maintenance', 'Base maintenance');
		$line_maintenance = icl_translate('NM Aitcraft', 'line-maintenance', 'Line maintenance');
		
		$mro_location_facilities = icl_translate('NM Aitcraft', 'mro-location-facilities', 'MRO Certifications and Facilities');
		$mro = icl_translate('NM Aitcraft', 'mro', 'MRO');
		$aircraft_ratings = icl_translate('NM Aitcraft', 'aircraft-ratings', 'Aircraft Ratings');
		$text_mro_capability = icl_translate('NM Aitcraft', 'mro-capability', 'MRO Capability');
		$text_mro_facility = icl_translate('NM Aitcraft', 'mro-facility', 'MRO Facility');
	?>
	<div class="with-sidebar-wrapper">
		<div class="with-sidebar-container container limoking-class-<?php echo $limoking_sidebar['type']; ?>">
				<div class="with-sidebar-content twelve columns">
					<div class="limoking-item limoking-item-start-content">
						<div id="aircraft-location-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="aircraft-location-info-wrapper" >
                            	<div class="limoking-item-title-wrapper limoking-item limoking-center limoking-medium ">
                                	<div class="limoking-item-title-container container">
                                    	<div class="limoking-item-title-head-inner">
                                        	<h3 class="limoking-item-title limoking-skin-title limoking-skin-border"><?php echo get_the_title().''.$mro;?></h3>
                                        </div>
                                    </div>
                                </div>
								
                                <div class="limoking-item limoking-content-item">
                                	<?php the_content();?>
                                </div>
								<div class="clear"></div>
								<div class="location_facilities">
                                <?php
								$facilities = get_field('facilities');

								if(count($facilities)){
									echo '<div class="limoking-item limoking-tab-item vertical">';
										echo '<div class="tab-title-wrapper">';
										$i=1;
										foreach($facilities as $facility){
											$ac_cls = ($i==1) ? ' active':'';
											echo '<h4 class="tab-title '.$ac_cls.'"><span>'.$facility['title'].'</span></h4>';
											$i++;
										}
										echo '</div>';
										echo '<div class="tab-content-wrapper">';
										$i=1;
										foreach($facilities as $facility){
											$ac_cls = ($i==1) ? ' active':'';
											echo '<div class="tab-content '.$ac_cls.'">';
											echo $facility['description'];
											echo '</div>';
											$i++;
										}
										echo '</div>';
										echo '<div class="clear"></div>';
									echo '</div>';
								}
								?>
                                </div><!-- .location_facilities -->
                                
                                <div class="limoking-item-title-wrapper limoking-item limoking-center limoking-medium ">
                                	<div class="limoking-item-title-container container">
                                    	<div class="limoking-item-title-head-inner">
                                        	<h3 class="limoking-item-title limoking-skin-title limoking-skin-border"><?php echo $aircraft_ratings;?></h3>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="aircraft_by_type">
                                	<div class="aircraft_info_top"><span class="mn_type_base"><?php echo $base_maintenance;?></span><span class="mn_type_line"><?php echo $line_maintenance;?></span></div>
                                <?php
								$aircraft_types = get_terms('aircraft_type', array('parent' => 0, 'hide_empty' => 0, 'orderby'=>"menu_order", 'order'      => 'ASC'));
								//echo '<pre>';print_r($aircraft_types);echo '</pre>';exit;
								?>
                                	<div class="limoking-item limoking-tab-item horizontal">
                                    	<div class="tab-title-wrapper">
                                        <?php 
										$i=1;
										$aircraft_count = array();
										foreach($aircraft_types as $aircraft_type){
											$has_service = 0;
											$aircraft_args = array(
												'posts_per_page' => -1,
												'tax_query' => array(
												'relation' => 'AND',
													array(
														'taxonomy' => 'aircraft_type',
														'field' => 'id',
														'terms' => $aircraft_type->term_id,
														'operator' => 'IN'
													)
												),
												'post_type' => 'aircraft',
												'orderby' => 'menu_order'
											);
											
											$aircrafts = new WP_Query($aircraft_args);
											if( $aircrafts->have_posts() ){
												$has_service = 0;
												while($aircrafts->have_posts()){
													$aircrafts->the_post();
													$aircraft = get_post();
													$aircraft_id = $aircraft->ID;
													
													$mro_locations = get_post_meta($aircraft_id,'mro_locations',true);

													if(is_array($mro_locations) && count($mro_locations)){
															foreach($mro_locations as $location_id=>$mro_location){
																if($_locationID==$location_id){
																	$has_service++;
																}
															}
													}
												}
											}
											wp_reset_query();
											if($has_service){
												$aircraft_count[$aircraft_type->term_id] = $has_service;
												$ac_cls = ($i==1) ? ' active':'';
												echo '<h4 class="tab-title '.$ac_cls.'" data-air="'.$has_service.'"><span>'.$aircraft_type->name.'</span></h4>';
												$i++;	
											}
											
										}
										?>
                                        </div>
                                        <div class="tab-content-wrapper">
                                        <?php 
										$i=1;
										foreach($aircraft_types as $aircraft_type){
											if(isset($aircraft_count[$aircraft_type->term_id]) && $aircraft_count[$aircraft_type->term_id]){
											$ac_cls = ($i==1) ? ' active':'';
											echo '<div class="tab-content '.$ac_cls.'">';
											$aircraft_args = array(
												'posts_per_page' => -1,
												'tax_query' => array(
												'relation' => 'AND',
													array(
														'taxonomy' => 'aircraft_type',
														'field' => 'id',
														'terms' => $aircraft_type->term_id,
														'operator' => 'IN'
													)
												),
												'post_type' => 'aircraft',
												'orderby' => 'menu_order'
											);
											
											$aircrafts = new WP_Query($aircraft_args);

											if( $aircrafts->have_posts() ){
												while($aircrafts->have_posts()){
													$aircrafts->the_post();
													$aircraft = get_post();
													$aircraft_id = $aircraft->ID;
													
													$mro_locations = get_post_meta($aircraft_id,'mro_locations',true);
													
													$html = '';
													
													$html .= '<div class="aircraft_info">';
														$html .= '';
														//$html .= '<h3 class="aircraft_title">'.get_the_title($aircraft_id).'</h3>';
														$html .= '<div class="aircraft_reg_info limoking-item limoking-accordion-item style-1">';
														if(is_array($mro_locations) && count($mro_locations)){
															$j=1;
															foreach($mro_locations as $location_id=>$mro_location){
																if($_locationID==$location_id){
																	$original_ID = icl_object_id($aircraft_id, 'any', false, $language_code );
																$ac_class = ($i==1)?'':'';
																$icon_class = 'icon-plus';
																//$ac_class = ($i==1)?' pre-active active':'';
																//$icon_class = ($i==1)?'icon-minus':'icon-plus';
																$html .= '<div class="accordion-tab '.$ac_class.'">';
																	$html .= '<h4 class="accordion-title"><i class="'.$icon_class.'"></i><span>'.get_the_title($original_ID).'</span></h4>';
																	$html .= '<div class="accordion-content">';
																		$html .= '<ul class="airctaft_loc_reg">';
																		if(isset($mro_location['reg']) && count($mro_location['reg'])){
																			foreach($mro_location['reg'] as $reg_id=>$reg){
																				$regInfo = get_term( $reg_id, 'aircraft_registration' );
																				if($regInfo->term_id){
																					$base_class = '';
																					$line_class = '';
																					if(isset($reg['type']) && count($reg['type'])){
																						foreach($reg['type'] as $reg_type){
																							$typeInfo = get_term( $reg_type, 'maintenance_type' );
																							if($typeInfo->slug && strpos($typeInfo->slug,'base')===0)
																								$base_class = 'maintenance_base';
																							if($typeInfo->slug && strpos($typeInfo->slug,'line')===0)
																								$line_class = 'maintenance_line';
																						}
																					}
																					$html .= '<li><span class="reg_icon"><i class="'.$base_class.'"></i><i class="'.$line_class.'"></i></span><span class="reg_name">'.$regInfo->name.'</span></li>';
																				}
																			}
																		}
																		$html .= '</ul>';
																	$html .= '</div>';
																$html .= '</div>';
																$j++;
																}
															}
														}
														$html .= '</div>';
													$html .= '</div>';
													
													echo $html;
												}
												wp_reset_query();
											}
											echo '</div>';
											$i++;
											}
										}
										?>
                                        </div>
                                        <div class="clear"></div>
                                	</div>
                                </div><!-- .aircraft_by_type -->
                                
                                <div class="limoking-item-title-wrapper limoking-item limoking-center limoking-medium location_capability_title">
                                	<div class="limoking-item-title-container container">
                                    	<div class="limoking-item-title-head-inner">
                                        	<h3 class="limoking-item-title limoking-skin-title limoking-skin-border"><?php echo $text_mro_capability;?></h3>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="location_capability">
                                <?php
								$mro_capability = get_field('mro_capability');
								if(count($mro_capability)){
									echo '<ul class="mro_capability_list">';
									foreach($mro_capability as $capability){
										echo '<li><i class="fa fa-check-circle-o"></i><span>'.$capability['capability'].'</span></li>';
									}
									echo '</ul>';
								}
								?>
                                </div><!-- .location_capability -->
                            </div>    
							<div class="clear"></div>
						</div><!-- #aircraft -->
						<?php //  ?>
						
						<div class="clear"></div>	
					</div>
            	</div>        
			<div class="clear"></div>
		</div>
		<?php $image = get_field('background_image');
		if( !empty($image) ){ ?>	
        <section id="location-content-section-2">
        	<div id="limoking-parallax-wrapper-1" class="limoking-parallax-wrapper limoking-background-image gdlr-show-all no-skin" data-bgspeed="0" style="background-image: url('<?php echo $image['url']; ?>'); background-repeat: repeat;">
            	<div class="container">
                	<div class="six columns">
                    	<div id="camo-form" class="limoking-item limoking-content-item">
                        	<?php the_field('form_shortcode'); ?>
                        </div>
                    </div>
                    <div class="six columns mro_facility_container">
                    	<div id="mro-facility-form" class="limoking-item limoking-content-item">
                        	<div class="location_mro_facilities">
                            	<h4><?php echo $text_mro_facility;?></h4>
                            	<?php
								$mro_facility = get_field('mro_facility');
								if(count($mro_facility)){
									echo '<ul class="mro_facility_list">';
									foreach($mro_facility as $facility){
										echo '<li><i class="fa fa-check-circle-o"></i><span>'.$facility['facility'].'</span></li>';
									}
									echo '</ul>';
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</section>	
		<?php } else { ?>
        <section id="location-content-section-2">
        	<div id="limoking-parallax-wrapper-1" class="limoking-parallax-wrapper limoking-background-image gdlr-show-all no-skin" data-bgspeed="0" style="background-image: url('<?php echo get_site_url();?>/wp-content/uploads/2016/12/intro-650x310-2.jpg'); background-repeat: repeat;">
            	<div class="container">
                	<div class="six columns">
                    	<div id="camo-form" class="limoking-item limoking-content-item">
                        	<?php echo do_shortcode('[contact-form-7 id="7166" title="AOG Support Request Form"]');?>
                        </div>
                    </div>
                    <div class="six columns mro_facility_container">
                    	<div id="mro-facility-form" class="limoking-item limoking-content-item">
                        	<div class="location_mro_facilities">
                            	<h4><?php echo $text_mro_facility;?></h4>
                            	<?php
								$mro_facility = get_field('mro_facility');
								if(count($mro_facility)){
									echo '<ul class="mro_facility_list">';
									foreach($mro_facility as $facility){
										echo '<li><i class="fa fa-check-circle-o"></i><span>'.$facility['facility'].'</span></li>';
									}
									echo '</ul>';
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</section>	
		<?php } ?>			
	</div>				

</div><!-- limoking-content -->

<?php
	}
	
	get_footer(); 
?>