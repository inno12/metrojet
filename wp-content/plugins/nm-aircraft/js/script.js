jQuery(document).ready(function(){
	//jQuery('select[name="aircraft_filter_type"]').val('');
	setTimeout(function(){
		jQuery('select[name="aircraft_filter_type"]').trigger('change');
	},1000);
	
	jQuery(document).on('change', 'select[name="aircraft_filter_type"]', function(){
		var aircraft_filter_type = jQuery('select[name="aircraft_filter_type"] option:selected').attr('value');
		console.log(aircraft_filter_type);
		jQuery('select[name="aircraft_filter_model"] option.ft_model').hide().remove();
		jQuery('select[name="aircraft_filter_model"]').val('');
		if(aircraft_filter_type){
			if(aircraft_filter_type=='aircraft_type'){
				var empty_text = jQuery('select[name="aircraft_filter_model"] > option.empty').attr('data-model');
				jQuery('select[name="aircraft_filter_model"] > option.empty').text(empty_text);
				var filter_options = '';
				jQuery('.aircraft_filter_model_all > option').each(function(){
					if(jQuery(this).hasClass('aircraft_category') || jQuery(this).hasClass('aircraft_post')){
						//filter_options += jQuery(this)[0].outerHTML;
						var optionVal = jQuery(this).attr('value');
						var optionClass = jQuery(this).attr('class');
						var optionText = jQuery(this).text();
						jQuery('select[name="aircraft_filter_model"]').append( '<option value="'+optionVal+'" class="'+optionClass+'">'+optionText+'</option>' );
						/*jQuery('select[name="aircraft_filter_model"]').append(jQuery('<option>', {
							class: optionClass,
							value: optionVal,
							text: optionText
						}));*/
					}
				});
				//jQuery('select[name="aircraft_filter_model"]').append(filter_options);
				jQuery('select[name="aircraft_filter_model"] option.aircraft_category').show();
				jQuery('select[name="aircraft_filter_model"] option.aircraft_post').show();
			}
			else{
				var empty_text = jQuery('select[name="aircraft_filter_model"] > option.empty').attr('data-location');
				jQuery('select[name="aircraft_filter_model"] > option.empty').text(empty_text);
				jQuery('.aircraft_filter_model_all > option').each(function(){
					if(jQuery(this).hasClass('aircraft_location')){
						var optionVal = jQuery(this).attr('value');
						var optionClass = jQuery(this).attr('class');
						var optionText = jQuery(this).text();
						jQuery('select[name="aircraft_filter_model"]').append( '<option value="'+optionVal+'" class="'+optionClass+'">'+optionText+'</option>' );
					}
				});
				jQuery('select[name="aircraft_filter_model"] option.aircraft_location').show();
			}
		}
		else{
		}
	});
	
	jQuery(document).on('click', '.aircraft_filter_submit', function(){
		var aircraft_filter_type = jQuery('select[name="aircraft_filter_type"]').val();
		var aircraft_filter_model = jQuery('select[name="aircraft_filter_model"]').val();
		if(aircraft_filter_type){
			if(aircraft_filter_model){
				var model_post = jQuery('select[name="aircraft_filter_model"] option:selected').hasClass('aircraft_post');
				if(!model_post){
					location = aircraft_filter_model;
				}
				else{
					jQuery.ajax({
						type: "post",
						dataType: "json",
						url: aircraft_ajax_script.ajaxurl,
						data: {action: "get_aircraft_data", model_id:aircraft_filter_model, model_type:aircraft_filter_type},
						beforeSend: function(){
							jQuery('.aircraft_filter_submit').attr('disabled',true);
							jQuery.blockUI({
								message: '<h4>Loading..</h4>'
							});
						},
						complete: function(){
							jQuery('.aircraft_filter_submit').removeAttr('disabled');
							jQuery.unblockUI();
						},
						success: function(json) {
							if(json['html']){
								jQuery('.aircraft-result-container').html(json['html']);
							}
						}
					});
				}
			}
			else{
				alert('Please select Model');
			}
		}
		else{
			alert('Please select Type');
		}
	});
	
	jQuery(document).on('click', '.aircraft_reg_info .accordion-title', function(){
		if(jQuery(this).parent().hasClass('active')){
			jQuery(this).find('i').removeClass('icon-minus').addClass('icon-plus');
			jQuery(this).parent().removeClass('pre-active active');
			jQuery(this).parent().find('.accordion-content').slideUp();
		}
		else{
			jQuery(this).find('i').removeClass('icon-plus').addClass('icon-minus');
			jQuery(this).parent().addClass('active');
			jQuery(this).parent().find('.accordion-content').slideDown();
		}
	});
});