<?php
if ( !defined('ABSPATH') )
	die('-1');

add_shortcode( 'aircraft-filter', 'nm_aircraft_filter' );

function nm_aircraft_filter( $args, $instance ) {
	wp_enqueue_style('aircraft_main_css', plugins_url() . '/nm-aircraft/css/style.css', false,'1.0');
	wp_enqueue_script('blockUI_js', plugins_url() . '/nm-aircraft/js/jquery.blockUI.js', array('jquery'), '2.70' );
	wp_enqueue_script('aircraft_main_js', plugins_url() . '/nm-aircraft/js/script.js', array('jquery'), '1.0' );
	wp_localize_script( 'aircraft_main_js', 'aircraft_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php'), 'pluginurl' => plugin_dir_url( __FILE__ ) ) );
	
	$filter_models = get_aircraft_filter_model();
	
	$html = '';
	$html .= '<div class="aircraft-filter-container">';
	$html .= '<div class="aircraft-filter-form">';
		$html .= '<select class="aircraft_filter_type" name="aircraft_filter_type" autocomplete="off">';
			/*$html .= '<option value="">'.__('By Aircraft Type -OR- By MRO Location', 'nm-aircraft').'</option>';*/
			$html .= '<option value="aircraft_type">'.__('By Aircraft Type', 'nm-aircraft').'</option>';
			$html .= '<option value="aircraft_location">'.__('By MRO Location', 'nm-aircraft').'</option>';
		$html .= '</select>';
		
		$html .= '<select class="aircraft_filter_model" name="aircraft_filter_model" autocomplete="off">';
			$html .= '<option class="empty" value="" data-model="'.__('Model', 'nm-aircraft').'" data-location="'.__('Location', 'nm-aircraft').'">'.__('Model', 'nm-aircraft').'</option>';
			/*if(count($filter_models)){
				foreach($filter_models as $filter_model){
					$filter_value = ($filter_model['class']=='post') ? $filter_model['id'] : $filter_model['link'];
					$html .= '<option class="ft_model aircraft_'.$filter_model['class'].'" value="'.$filter_value.'">'.$filter_model['title'].'</option>';
				}
			}*/
		$html .= '</select>';
		
		$html .= '<button class="aircraft_filter_submit" type="button"></button>';
	$html .= '</div>';
	$html .= '</div>';
	
	$html .= '<div style="display:none;">';
		$html .= '<select class="aircraft_filter_model_all" autocomplete="off">';
			$html .= '<option class="empty" value="">'.__('Location', 'nm-aircraft').'</option>';
			if(count($filter_models)){
				foreach($filter_models as $filter_model){
					$filter_value = ($filter_model['class']=='post') ? $filter_model['id'] : $filter_model['link'];
					$html .= '<option class="ft_model aircraft_'.$filter_model['class'].'" value="'.$filter_value.'">'.$filter_model['title'].'</option>';
				}
			}
		$html .= '</select>';
	$html .= '</div>';
	
	$html .= '<div class="aircraft-result-container">';
	$html .= '</div>';
	
	return $html;
}