<?php 
	get_header('aircraft'); 
	
	global $limoking_contact_car, $wp_query;
	$limoking_contact_car = clone $wp_query;
	
	wp_enqueue_style('aircraft_main_css', plugins_url() . '/nm-aircraft/css/style.css', false,'1.0');
	
	//while( have_posts() )
	{ the_post();
?>
<div class="limoking-content aircraft-type-content">

	<?php 
		global $limoking_sidebar, $theme_option, $limoking_post_option, $limoking_is_ajax;
		
		
		$limoking_sidebar = array(
			'type'=> 'no-sidebar',
			'left-sidebar'=> '', 
			'right-sidebar'=> ''
		); 				
		//$limoking_sidebar = limoking_get_sidebar_class($limoking_sidebar);
		
		$language_code = ICL_LANGUAGE_CODE;
		
		icl_register_string( 'NM Aitcraft', 'base-maintenance', 'Base maintenance');
		icl_register_string( 'NM Aitcraft', 'line-maintenance', 'Line maintenance');
		$base_maintenance = icl_translate('NM Aitcraft', 'base-maintenance', 'Base maintenance');
		$line_maintenance = icl_translate('NM Aitcraft', 'line-maintenance', 'Line maintenance');
	?>
	<div class="with-sidebar-wrapper">
		<div class="with-sidebar-container container limoking-class-<?php echo $limoking_sidebar['type']; ?>">
				<div class="with-sidebar-content twelve columns">
					<div class="limoking-item limoking-item-start-content">
						<div id="aircraft-type-archive">
							<div class="aircraft-type-info-wrapper" >

                                <div class="aircraft_by_type">
                                	<div class="aircraft_info_top"><span class="mn_type_base"><?php echo $base_maintenance;?></span><span class="mn_type_line"><?php echo $line_maintenance;?></span></div>
                                <?php
								//$aircraft_types = get_terms('aircraft_type', array('parent' => 0, 'hide_empty' => 0, 'orderby'=>"id", 'order'      => 'ASC'));
								?>
                                	
                                        <?php 
										$aircraft_type = get_queried_object();
										if($aircraft_type->term_id){
											$aircraft_args = array(
												'posts_per_page' => -1,
												'tax_query' => array(
												'relation' => 'AND',
													array(
														'taxonomy' => 'aircraft_type',
														'field' => 'id',
														'terms' => $aircraft_type->term_id,
														'operator' => 'IN'
													)
												),
												'post_type' => 'aircraft',
												'orderby' => 'menu_order'
											);
											
											$aircrafts = new WP_Query($aircraft_args);
											if( $aircrafts->have_posts() ){
												while($aircrafts->have_posts()){
													$aircrafts->the_post();
													$aircraft = get_post();
													$aircraft_id = $aircraft->ID;
													
													$mro_locations = get_post_meta($aircraft_id,'mro_locations',true);

													$new_mro_locations = array();
													if(is_array($mro_locations) && count($mro_locations)){
														foreach($mro_locations as $location_id=>$mro_location){
															$original_ID = icl_object_id($location_id, 'any', false, $language_code );
															$sort_order = get_post_field( 'menu_order', $original_ID);
															$new_mro_locations[$location_id] = $sort_order;
															
															$new_reg = array();
															foreach($mro_location['reg'] as $reg_id=>$reg){
																$regInfo = get_term( $reg_id, 'aircraft_registration' );
																$new_reg[$reg_id] = $regInfo->term_order;
															}
															asort($new_reg);
															$reg_clone = array();
															foreach($new_reg as $key=>$order){
																$reg_clone[$key] = $mro_location['reg'][$key];
															}
															$mro_locations[$location_id]['reg'] = $reg_clone;
														}
														asort($new_mro_locations);
														//array_multisort($new_mro_locations, SORT_ASC, $mro_locations);
														
														$mro_locations_clone = array();
														foreach($new_mro_locations as $key=>$order){
															$mro_locations_clone[$key] = $mro_locations[$key];
														}
														$mro_locations = $mro_locations_clone;
													}
													
													$html = '';
													
													$html .= '<div class="aircraft_info">';
														$html .= '';
														$html .= '<h3 class="aircraft_title">'.get_the_title($aircraft_id).'</h3>';
														$html .= '<div class="aircraft_reg_info limoking-item limoking-accordion-item style-1">';
														if(is_array($mro_locations) && count($mro_locations)){
															$i=1;
															foreach($mro_locations as $location_id=>$mro_location){
																$ac_class = ($i==1)?'':'';
																$icon_class = 'icon-plus';
																//$ac_class = ($i==1)?' pre-active active':'';
																//$icon_class = ($i==1)?'icon-minus':'icon-plus';
																$html .= '<div class="accordion-tab '.$ac_class.'">';
																	$original_ID = icl_object_id($location_id, 'any', false, $language_code );
																	$html .= '<h4 class="accordion-title"><i class="'.$icon_class.'"></i><span>'.get_the_title($original_ID).'</span></h4>';
																	$html .= '<div class="accordion-content">';
																		$html .= '<ul class="airctaft_loc_reg">';
																		if(isset($mro_location['reg']) && count($mro_location['reg'])){
																			foreach($mro_location['reg'] as $reg_id=>$reg){
																				$regInfo = get_term( $reg_id, 'aircraft_registration' );
																				if($regInfo->term_id){
																					$base_class = '';
																					$line_class = '';
																					if(isset($reg['type']) && count($reg['type'])){
																						foreach($reg['type'] as $reg_type){
																							//$original_reg_type_ID = icl_object_id( $reg_type, 'maintenance_type', true, 'en' );
																							$typeInfo = get_term( $reg_type, 'maintenance_type' );
																							if($typeInfo->slug && strpos($typeInfo->slug,'base')===0)
																								$base_class = 'maintenance_base';
																							if($typeInfo->slug && strpos($typeInfo->slug,'line')===0)
																								$line_class = 'maintenance_line';
																						}
																					}
																					$html .= '<li><span class="reg_icon"><i class="'.$base_class.'"></i><i class="'.$line_class.'"></i></span><span class="reg_name">'.$regInfo->name.'</span></li>';
																				}
																			}
																		}
																		$html .= '</ul>';
																	$html .= '</div>';
																$html .= '</div>';
																$i++;
															}
														}
														$html .= '</div>';
													$html .= '</div>';
													
													echo $html;
												}
												wp_reset_query();
											}
											$i++;
										}
										?>
                                        <div class="clear"></div>
                                </div><!-- .aircraft_by_type -->

                            </div>    
							<div class="clear"></div>
						</div><!-- #aircraft -->
						<?php //  ?>
						
						<div class="clear"></div>	
					</div>
            	</div>        
			<div class="clear"></div>
		</div>	
		<?php $image = get_field('background_image', $aircraft_type);
		if( !empty($image) ){ ?>
        <section id="location-content-section-2">
        	<div id="limoking-parallax-wrapper-1" class="limoking-parallax-wrapper limoking-background-image gdlr-show-all no-skin" data-bgspeed="0" style="background-image: url('<?php echo $image['url']; ?>'); background-repeat: repeat;">
            	<div class="container">
                	<div class="six columns">
                    	<div id="camo-form" class="limoking-item limoking-content-item">
                        	<?php the_field('form_shortcode', $aircraft_type); ?>
                        </div>
                    </div>
                </div>
            </div>
		</section>	
		<?php } else { ?>
        <section id="location-content-section-2">
        	<div id="limoking-parallax-wrapper-1" class="limoking-parallax-wrapper limoking-background-image gdlr-show-all no-skin" data-bgspeed="0" style="background-image: url('<?php echo get_site_url();?>/wp-content/uploads/2016/12/intro-650x310-2.jpg'); background-repeat: repeat;">
            	<div class="container">
                	<div class="six columns">
                    	<div id="camo-form" class="limoking-item limoking-content-item">
                        	<?php echo do_shortcode('[contact-form-7 id="7166" title="AOG Support Request Form"]');?>
                        </div>
                    </div>
                </div>
            </div>
		</section>	
		<?php } ?>	
	</div>				

</div><!-- limoking-content -->

<?php
	}
	
	get_footer(); 
?>