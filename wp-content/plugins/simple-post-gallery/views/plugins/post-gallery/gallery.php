<?php
/**
 * Gallery view/template.
 *
 * @author Alejandro Mostajo <http://about.me/amostajo>
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGallery
 * @version 1.0
 */ 
?>
<div class="post-gallery flexslider">
 <ul class="slides">
    <?php foreach ( $post->gallery as $attachment ) : ?>
<li>
        <img src="<?php echo $attachment->thumb_url ?>"
                alt="<?php echo $attachment->alt ?>"
            />
</li>
    <?php endforeach ?>
</ul>
</div>