<?php
show_admin_bar(false);
if( function_exists('icl_register_string') ){	
	icl_register_string( 'default', 'parination-prev', '&lsaquo; Previous');
	icl_register_string( 'default', 'parination-next', 'Next &rsaquo;');
}
if( !function_exists('limoking_get_pagination') ){	
	function limoking_get_pagination($max_num_page, $current_page, $format = 'paged'){
		if( $max_num_page <= 1 ) return '';
		
		$prev = icl_translate('default', 'parination-prev', '&lsaquo; Previous');
		$next = icl_translate('default', 'parination-next', 'Next &rsaquo;');
		
		$big = 999999999; // need an unlikely integer
		return 	'<div class="limoking-pagination">' . paginate_links(array(
			'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format' => '?' . $format . '=%#%',
			'current' => max(1, $current_page),
			'total' => $max_num_page,
			'prev_text'=> $prev,
			'next_text'=> $next
		)) . '</div>';
	}	
}		
if( !function_exists('limoking_get_ajax_pagination') ){	
	function limoking_get_ajax_pagination($max_num_page, $current_page){
		if( $max_num_page <= 1 ) return '';
		
		$prev = icl_translate('default', 'parination-prev', '&lsaquo; Previous');
		$next = icl_translate('default', 'parination-next', 'Next &rsaquo;');
		
		$ret  = '<div class="limoking-pagination limoking-ajax">';
		if($current_page > 1){ 
			$ret .= '<a class="prev page-numbers" data-paged="' . (intval($current_page) - 1) . '" >';
			$ret .= $prev . '</a> '; 
		}
		for($i=1; $i<=$max_num_page; $i++){
			if( $i == $current_page ){
				$ret .= '<span class="page-numbers current" data-paged="' . $i . '" >' . $i . '</span> ';
			}else{
				$ret .= '<a class="page-numbers" data-paged="' . $i . '" >' . $i . '</a> ';
			}
		}
		if($current_page < $max_num_page){ 
			$ret .= '<a class="next page-numbers" data-paged="' . (intval($current_page) + 1) . '" > ';
			$ret .= $next . '</a> '; 
		}
		$ret .= '</div>';
		return $ret;
	}	
}
?>